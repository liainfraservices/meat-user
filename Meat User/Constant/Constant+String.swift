//
//  Constant+String.swift
//  Taxi App
//
//  Created by Pyramidions on 30/01/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import Foundation
import Localize_Swift

typealias CONSTANT = String

extension CONSTANT
{
    public static var UNAUTHORIZED = "Session expired. Please login again".localized()
    public static var APP_NAME = "Meat user".localized()
    public static var OK = "Ok".localized()
    public static var OOPS = "Oops".localized()
    public static var ERROR = "Error".localized()
    public static var CHECK_INTERNET_CONNECTION = "Check Internet Connection".localized()
    public static var SUCCESS = "Success".localized()
    public static var SIGN_IN = "Sign in".localized()
    public static var NAME_EMAIL = "E-mail".localized()
    public static var PASSWORD = "Password".localized()
    public static var FORGOT_PASSWORD = "Forgot Password".localized()
    public static var REGISTER_LATER = "Register now".localized()
    public static var REGISTER = "Register".localized()
    public static var NAME = "Name".localized()
    public static var EMAIL = "Email".localized()
    public static var MOBILE_NO = "Mobile number".localized()
    public static var CONFIRM_PASSWORD = "Confirm password".localized()
    public static var ALREADY_ACCOUNT = "Already have an account".localized()
    public static var SIGN_UP = "Sign up".localized()
    public static var ENTER_NAME = "Enter your name or e-mail".localized()
    public static var ENTER_PASSWORD = "Enter your password".localized()
    public static var ENTER_NEW_PASSWORD = "Enter your new password".localized()
    public static var ENTER_NAME_REGISTER = "Enter your name".localized()
    public static var ENTER_EMAIL = "Enter your email".localized()
    public static var PASSWORD_SAME = "Password and confirm password should be same".localized()
    public static var ENTER_CONFIRM_PASSWORD = "Enter your confirm password".localized()
    public static var ENTER_MOBILE = "Enter your mobile number".localized()
    public static var START = "Start".localized()
    public static var PROFILE = "Profile".localized()
    public static var EDIT_PROFILE = "Edit Profile".localized()
    public static var NEW_PASSWORD = "New password".localized()
    public static var CURRENT_PASSWORD = "Current password".localized()
    public static var SAVE = "Save".localized()
    public static var LOGOUT = "Logout".localized()
    public static var LOGIN = "Login".localized()
    public static var PASSWORD_NOT_SAME = "New Password and current password should not same".localized()
    public static var SIZE = "Size".localized()
    public static var CHIPPING = "Cutting Style".localized()
    public static var CUTTING = "cutting".localized()
    public static var IN = "in".localized()
    public static var QUANTITY = "Quantity".localized()
    public static var KHEEMA = "Kheema".localized()
    public static var PACKING_STYLE = "Packing Style".localized()
    public static var PACKING = "packing".localized()
    public static var WITH = "with".localized()
    public static var CONTINUE = "Continue".localized()
    public static var ADD_TO_CART = "Add to cart".localized()
    public static var UPDATE_TO_CART = "Update to cart".localized()
    public static var MINCED_MEAT = "Minced meat".localized()
    public static var ENCAPSULATION = "Encapsulation".localized()
    public static var CITY = "City".localized()
    public static var BRANCH = "Branch".localized()
    public static var VALUE = "Value".localized()
    public static var VAT = "VAT".localized()
    public static var TOTAL = "Total".localized()
    public static var DELETE = "Delete".localized()
    public static var FOLLOWUP = "Place order".localized()
    public static var PICK_UP_LOCATION = "Select your address".localized()
    public static var SERACHBAR_PLACEHOLDER = "Search your address".localized()
    public static var INVALID_REQUEST = "Invalid request".localized()
    public static var CONFIRM_LOCATION = "Confirm location".localized()
    public static var STREET = "Street".localized()
    public static var NAME_OF_BUILDING = "No. / Name of the building or villa to pay".localized()
    public static var FLOOR = "Floor".localized()
    public static var FLAT = "Flat".localized()
    public static var CONTINUE_TO_PAY = "Continue to pay".localized()
    public static var NOTES = "Notes".localized()
    public static var THANK_YOU = "Thank you".localized()
    public static var APPLICATION_NO = "Application number".localized()
    public static var ORDER_CONFIRMED = "The order has been confirmed".localized()
    public static var GUEST_USER = "Continue as Guest user".localized()
    public static var DASHBOARD = "Dashboard".localized()
    public static var TERMS_AND_COND = "Terms and conditions".localized()
    public static var HELP_CONTACT = "Help and contact us".localized()
    public static var CHANGE_PASSWORD = "Change password".localized()
    public static var SELECT_BRANCH = "Please select a branch".localized()
    public static var SELECT_BRANCH_TITLE = "Select branch".localized()
    public static var CONTACT_US = "Contact us".localized()
    public static var YOU_CAN_CONTACT = "You can reach us at".localized()
    public static var ADD_ADDRESS = "Add Address".localized()
    public static var LOGIN_ACCESS_FEATURES = "Please login to access the features".localized()
    public static var PLEASE_ENTER_NOTES = "Please enter the notes".localized()
    public static var ADDRESS = "Address".localized()
    public static var PHONE_NUMBER = "Phone number".localized()
    public static var YOU_NEED_PICK_LOCATION = "You need to pick the exaction location from map".localized()
    public static var PLEASE_CHOOSE_ADDRESS = "Please choose address".localized()
    public static var ENTER_PHONE_NO = "Please enter phone number".localized()
    public static var ENTER_NAME_OF_BUILDING = "Please enter name of building".localized()
    public static var PLEASE_ENTER_FLAT_NO = "Please enter flat no".localized()
    public static var SAVED_ADDRESS = "Save address".localized()
    public static var CONFIRMED_PROCEESED = "Your order has been created".localized()
    public static var COD = "Cash on delivery".localized()
    public static var COC = "Cash on card".localized()
    public static var PLEASE_LOGIN = "Please login".localized()
    public static var DO_YOU_WANT_TO_LOGIN = "Please login to access the features. Would you like to login your account?".localized()
    public static var CANCEL = "Cancel".localized()
    public static var PINLOCATION = "Pin location".localized()
    public static var SUBTOTAL = "Sub total".localized()
    public static var QTY = "Qty".localized()
    public static var ORDER_ID = "Order ID".localized()
    public static var SUCCESS_LOGOUT = "You have logout successfully".localized()
    public static var NO_PRODUCT = "No products available".localized()
    public static var PLEASE_SELECT = "Please select".localized()
    public static var BARNCH_DES = "Please select any one of the branch".localized()
}

func setText()
{
    CONSTANT.UNAUTHORIZED = "Session expired. Please login again".localized()
    CONSTANT.APP_NAME = "Meat user".localized()
    CONSTANT.OK = "Ok".localized()
    CONSTANT.OOPS = "Oops".localized()
    CONSTANT.ERROR = "Error".localized()
    CONSTANT.CHECK_INTERNET_CONNECTION = "Check Internet Connection".localized()
    CONSTANT.SUCCESS = "Success".localized()
    CONSTANT.SIGN_IN = "Sign in".localized()
    CONSTANT.NAME_EMAIL = "E-mail".localized()
    CONSTANT.PASSWORD = "Password".localized()
    CONSTANT.FORGOT_PASSWORD = "Forgot Password".localized()
    CONSTANT.REGISTER_LATER = "Register now".localized()
    CONSTANT.REGISTER = "Register".localized()
    CONSTANT.NAME = "Name".localized()
    CONSTANT.EMAIL = "Email".localized()
    CONSTANT.MOBILE_NO = "Mobile number".localized()
    CONSTANT.CONFIRM_PASSWORD = "Confirm password".localized()
    CONSTANT.ALREADY_ACCOUNT = "Already have an account".localized()
    CONSTANT.SIGN_UP = "Sign up".localized()
    CONSTANT.ENTER_NAME = "Enter your name or e-mail".localized()
    CONSTANT.ENTER_PASSWORD = "Enter your password".localized()
    CONSTANT.ENTER_NEW_PASSWORD = "Enter your new password".localized()
    CONSTANT.ENTER_NAME_REGISTER = "Enter your name".localized()
    CONSTANT.ENTER_EMAIL = "Enter your email".localized()
    CONSTANT.PASSWORD_SAME = "Password and confirm password should be same".localized()
    CONSTANT.ENTER_CONFIRM_PASSWORD = "Enter your confirm password".localized()
    CONSTANT.ENTER_MOBILE = "Enter your mobile number".localized()
    CONSTANT.START = "Start".localized()
    CONSTANT.PROFILE = "Profile".localized()
    CONSTANT.EDIT_PROFILE = "Edit Profile".localized()
    CONSTANT.NEW_PASSWORD = "New password".localized()
    CONSTANT.CURRENT_PASSWORD = "Current password".localized()
    CONSTANT.SAVE = "Save".localized()
    CONSTANT.LOGOUT = "Logout".localized()
    CONSTANT.LOGIN = "Login".localized()
    CONSTANT.PASSWORD_NOT_SAME = "New Password and current password should not same".localized()
    CONSTANT.SIZE = "Size".localized()
    CONSTANT.CHIPPING = "Cutting Style".localized()
    CONSTANT.CUTTING = "cutting".localized()
    CONSTANT.IN = "in".localized()
    CONSTANT.QUANTITY = "Quantity".localized()
    CONSTANT.KHEEMA = "Kheema".localized()
    CONSTANT.PACKING_STYLE = "Packing Style".localized()
    CONSTANT.PACKING = "packing".localized()
    CONSTANT.WITH = "with".localized()
    CONSTANT.CONTINUE = "Continue".localized()
    CONSTANT.ADD_TO_CART = "Add to cart".localized()
    CONSTANT.UPDATE_TO_CART = "Update to cart".localized()
    CONSTANT.MINCED_MEAT = "Minced meat".localized()
    CONSTANT.ENCAPSULATION = "Encapsulation".localized()
    CONSTANT.CITY = "City".localized()
    CONSTANT.BRANCH = "Branch".localized()
    CONSTANT.VALUE = "Value".localized()
    CONSTANT.VAT = "VAT".localized()
    CONSTANT.TOTAL = "Total".localized()
    CONSTANT.DELETE = "Delete".localized()
    CONSTANT.FOLLOWUP = "Place order".localized()
    CONSTANT.PICK_UP_LOCATION = "Select your address".localized()
    CONSTANT.SERACHBAR_PLACEHOLDER = "Search your address".localized()
    CONSTANT.INVALID_REQUEST = "Invalid request".localized()
    CONSTANT.CONFIRM_LOCATION = "Confirm location".localized()
    CONSTANT.STREET = "Street".localized()
    CONSTANT.NAME_OF_BUILDING = "No. / Name of the building or villa to pay".localized()
    CONSTANT.FLOOR = "Floor".localized()
    CONSTANT.FLAT = "Flat".localized()
    CONSTANT.CONTINUE_TO_PAY = "Continue to pay".localized()
    CONSTANT.NOTES = "Notes".localized()
    CONSTANT.THANK_YOU = "Thank you".localized()
    CONSTANT.APPLICATION_NO = "Application number".localized()
    CONSTANT.ORDER_CONFIRMED = "The order has been confirmed".localized()
    CONSTANT.GUEST_USER = "Continue as Guest user".localized()
    CONSTANT.DASHBOARD = "Dashboard".localized()
    CONSTANT.TERMS_AND_COND = "Terms and conditions".localized()
    CONSTANT.HELP_CONTACT = "Help and contact us".localized()
    CONSTANT.CHANGE_PASSWORD = "Change password".localized()
    CONSTANT.SELECT_BRANCH = "Please select a branch".localized()
    CONSTANT.SELECT_BRANCH_TITLE = "Select branch".localized()
    CONSTANT.CONTACT_US = "Contact us".localized()
    CONSTANT.YOU_CAN_CONTACT = "You can reach us at".localized()
    CONSTANT.ADD_ADDRESS = "Add Address".localized()
    CONSTANT.LOGIN_ACCESS_FEATURES = "Please login to access the features".localized()
    CONSTANT.PLEASE_ENTER_NOTES = "Please enter the notes".localized()
    CONSTANT.ADDRESS = "Address".localized()
    CONSTANT.PHONE_NUMBER = "Phone number".localized()
    CONSTANT.YOU_NEED_PICK_LOCATION = "You need to pick the exaction location from map".localized()
    CONSTANT.PLEASE_CHOOSE_ADDRESS = "Please choose address".localized()
    CONSTANT.ENTER_PHONE_NO = "Please enter phone number".localized()
    CONSTANT.ENTER_NAME_OF_BUILDING = "Please enter name of building".localized()
    CONSTANT.PLEASE_ENTER_FLAT_NO = "Please enter flat no".localized()
    CONSTANT.SAVED_ADDRESS = "Save address".localized()
    CONSTANT.CONFIRMED_PROCEESED = "Your order has been created".localized()
    CONSTANT.COD = "Cash on delivery".localized()
    CONSTANT.COC = "Cash on card".localized()
    CONSTANT.PLEASE_LOGIN = "Please login".localized()
    CONSTANT.DO_YOU_WANT_TO_LOGIN = "Please login to access the features. Would you like to login your account?".localized()
    CONSTANT.CANCEL = "Cancel".localized()
    CONSTANT.PINLOCATION = "Pin location".localized()
    CONSTANT.SUBTOTAL = "Sub total".localized()
    CONSTANT.QTY = "Qty".localized()
    CONSTANT.ORDER_ID = "Order ID".localized()
    CONSTANT.SUCCESS_LOGOUT = "You have logout successfully".localized()
    CONSTANT.NO_PRODUCT = "No products available".localized()
    CONSTANT.PLEASE_SELECT = "Please select".localized()
    CONSTANT.BARNCH_DES = "Please select any one of the branch".localized()
}
