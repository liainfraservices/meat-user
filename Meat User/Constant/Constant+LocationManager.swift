import UIKit
import CoreLocation

protocol LocationServiceDelegate
{
    func tracingLocation(currentLocation: CLLocation)
    func tracingLocationDidFailWithError(error: NSError)
    func tracingLocationCompleteAddress(address: String,latitude : String, longitude :String)

}

class LocationManager: NSObject,CLLocationManagerDelegate
{
    var locationManager: CLLocationManager?
    var lastLocation: CLLocation?
    var delegate: LocationServiceDelegate?
    
    override init()
    {
        super.init()
        self.locationManager = CLLocationManager()
        
        guard let locationManagers=self.locationManager else
        {
            return
        }
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManagers.requestAlwaysAuthorization()
            locationManagers.requestWhenInUseAuthorization()
        }
        if #available(iOS 9.0, *) {
            //            locationManagers.allowsBackgroundLocationUpdates = true
        } else {
            // Fallback on earlier versions
        }
        locationManagers.desiredAccuracy = kCLLocationAccuracyBest
        locationManagers.pausesLocationUpdatesAutomatically = false
        locationManagers.distanceFilter = 0.1
        locationManagers.delegate = self
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = locations.last else
        {
            return
        }
        self.lastLocation = location
        self.getAdressName(coords: location)
        
        updateLocation(currentLocation: location)
    }
    
    @nonobjc func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            locationManager?.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager?.startUpdatingLocation()
            break
        case .authorizedAlways:
            locationManager?.startUpdatingLocation()
            break
        case .restricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    
    private func updateLocation(currentLocation: CLLocation)
    {
        guard let delegate = self.delegate else
        {
            return
        }
        delegate.tracingLocation(currentLocation: currentLocation)
    }
    
    private func updateLocationDidFailWithError(error: NSError)
    {
        guard let delegate = self.delegate else
        {
            return
        }
        delegate.tracingLocationDidFailWithError(error: error)
    }
    
    func startUpdatingLocation()
    {
        self.locationManager?.startUpdatingLocation()
    }
    
    func stopUpdatingLocation()
    {
        self.locationManager?.stopUpdatingLocation()
    }
    
    func startMonitoringSignificantLocationChanges()
    {
        self.locationManager?.startMonitoringSignificantLocationChanges()
    }
    
    func getAdressName(coords: CLLocation)
    {
        CLGeocoder().reverseGeocodeLocation(coords)
        {
            (placemark, error) in
            if error != nil
            {
                print("Hay un error")
            }
            else
            {
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + ", "
                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + ", "
                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                    
                    self.delegate?.tracingLocationCompleteAddress(address: adressString, latitude: String(describing: coords.coordinate.latitude), longitude: String(describing: coords.coordinate.longitude))
                    
//                    self.lblPlace.text = adressString
                }
            }
        }
    }

    
}
