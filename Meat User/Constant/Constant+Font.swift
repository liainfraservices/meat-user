//
//  Constant+Font.swift
//  Taxi App
//
//  Created by Pyramidions on 30/01/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import Foundation
import UIKit

enum Language: String
{
    case english = "en"
    case arabic = "ar"
}

enum OpenSansFontBook: String
{
    case RegularFont = "OpenSans-Regular"
    case MediumFont = "OpenSans-SemiBold"
    case BoldFont = "OpenSans-Bold"
}

enum DroidKulfiFontBook: String
{
    case RegularFont = "DroidArabicKufi"
    case BoldFont = "DroidArabicKufi-Bold"
}


extension UIFont
{
    class func appRegularFontWith( size:CGFloat) -> UIFont
    {
//        let size = size - 2
        
        if LANGUAGE == .english
        {
            return  UIFont(name: OpenSansFontBook.RegularFont.rawValue, size: size)!
        }
        else
        {
            return  UIFont(name: DroidKulfiFontBook.RegularFont.rawValue, size: size)!
        }
    }
    
    class func appMediumFontWith( size:CGFloat) -> UIFont
    {
//        let size = size - 2

        if LANGUAGE == .english
        {
            return  UIFont(name: OpenSansFontBook.MediumFont.rawValue, size: size)!
        }
        else
        {
            return  UIFont(name: DroidKulfiFontBook.RegularFont.rawValue, size: size)!
        }
    }
    
    class func appBoldFontWith( size:CGFloat) -> UIFont
    {
//        let size = size - 2

        if LANGUAGE == .english
        {
            return  UIFont(name: OpenSansFontBook.BoldFont.rawValue, size: size)!
        }
        else
        {
            return  UIFont(name: DroidKulfiFontBook.BoldFont.rawValue, size: size)!
        }
    }
}

