//
//  Constant+Extension.swift
//  MVCDemo
//
//  Created by Pyramidions on 24/09/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import Foundation
import UIKit
//import TransitionButton
import CoreLocation
//import GoogleMaps
//import GooglePlaces

extension UIViewController
{
    
    func statusBarNightModeOn()
    {
        var preferredStatusBarStyle: UIStatusBarStyle
        {
            return .lightContent
        }
    }
    
    func statusBarNightModeOff()
    {
        var preferredStatusBarStyle: UIStatusBarStyle
        {
            return .default
        }
    }
    
    func showAlertError(titleStr:String = CONSTANT.ERROR, messageStr:String)
    {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: CONSTANT.OK, style: UIAlertAction.Style.default, handler: nil))
        
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: titleStr as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(myMutableString, forKey: "attributedTitle")
        
        var messageMutableString = NSMutableAttributedString()
        messageMutableString = NSMutableAttributedString(string: messageStr as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(messageMutableString, forKey: "attributedMessage")

        self.present(alert, animated: true, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func userInteractionFalse()
    {
        self.view.isUserInteractionEnabled = false
    }

    func userInteractionTrue()
    {
        self.view.isUserInteractionEnabled = true
    }
}




extension UIView
{
    
    func dropShadow()
    {
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 2, height: 2)
    }
    
    func dropBottomShadow()
    {
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 4
        
    }
    
    func dropTopShadow()
    {        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 2, height: 0)
        layer.shadowRadius = 4
        
    }
    
    func addshadow(top: Bool, left: Bool, bottom: Bool, right: Bool, shadowRadius: CGFloat = 2.0) {
            
            layer.masksToBounds = false
            layer.shadowColor =  UIColor.lightGray.cgColor
            layer.shadowOffset = CGSize(width: 0.0, height: 1)
            layer.shadowRadius = shadowRadius
            layer.shadowOpacity = 2.0
            
            let path = UIBezierPath()
            var x: CGFloat = 0
            var y: CGFloat = 0
            var viewWidth = self.frame.width
            var viewHeight = self.frame.height
            
            // here x, y, viewWidth, and viewHeight can be changed in
            // order to play around with the shadow paths.
            if (!top) {
                y+=(shadowRadius+1)
            }
            if (!bottom) {
                viewHeight-=(shadowRadius+1)
            }
            if (!left) {
                x+=(shadowRadius+1)
            }
            if (!right) {
                viewWidth-=(shadowRadius+1)
            }
            // selecting top most point
            path.move(to: CGPoint(x: x, y: y))
            // Move to the Bottom Left Corner, this will cover left edges
            /*
             |☐
             */
            path.addLine(to: CGPoint(x: x, y: viewHeight))
            // Move to the Bottom Right Corner, this will cover bottom edge
            /*
             ☐
             -
             */
            path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
            // Move to the Top Right Corner, this will cover right edge
            /*
             ☐|
             */
            path.addLine(to: CGPoint(x: viewWidth, y: y))
            // Move back to the initial point, this will cover the top edge
            /*
             _
             ☐
             */
            path.close()
            //layer.shadowPath = path.cgPath
        }
    
    func shake()
    {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.5
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func activityStartAnimating(activityColor: UIColor, backgroundColor: UIColor)
    {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        
        backgroundView.addSubview(activityIndicator)
        
        self.addSubview(backgroundView)
    }
    
    func activityStopAnimating()
    {
        if let background = viewWithTag(475647)
        {
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
    }
    
    func hideAndShow(view: UIView, hidden: Bool)
    {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations:
        {
            view.isHidden = hidden
        })
    }
    
    
    func hideShow(hidden: Bool)
    {
        UIView.transition(with: self, duration: 0.5, options: .transitionCrossDissolve, animations:
        {
            self.isHidden = hidden
        })
    }
    
    func showCountryView()  {
     
        UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: .calculationModeCubicPaced, animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
               
                self.isHidden = false
                self.transform = .identity
                
            }
            
        }, completion: { finished in
            
        })
        
    }
    
    func hideCountryView()
    {
        UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: .calculationModeCubicPaced, animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
             
                self.transform = CGAffineTransform(scaleX: 0, y: 0)
            }
        }, completion: { finished in
            
            self.isHidden = true
        })
    }
    
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in })
    {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            self.alpha = 1.0
            completion(true)
        }
    }

}


extension UIButton
{
    func hideAndShow(hidden: Bool)
    {
        UIView.transition(with: self, duration: 0.3, options: .transitionFlipFromTop, animations:
        {
                self.isHidden = hidden
        })
    }
    
    func buttonShake()
    {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}

extension UIPageControl
{
    func hideAndShow(hidden: Bool)
    {
        UIView.transition(with: self, duration: 0.3, options: .transitionCrossDissolve, animations:
        {
                self.isHidden = hidden
        })
    }
}

extension UIColor
{
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension String
{
    func isValidEmail() -> Bool
    {
/*        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil*/
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)

    }
    
    /*
    func isValidPassword() -> (Bool,String)
    {
        if self.isEmpty
        {
            return (false,CONSTANT.VAILD_PASSWORD)
        }
        else if !VALID_UPPER_CASE.evaluate(with: self)
        {
            return (false,CONSTANT.VALID_PASSWORD_UPPER_CASE)
        }
        else if !VALID_LOWER_CASE.evaluate(with: self)
        {
            return (false,CONSTANT.VALID_PASSWORD_LOWER_CASE)
        }
        else if self.rangeOfCharacter(from: VALID_SPECIAL_CHAR_CASE.inverted) == nil
        {
            return (false,CONSTANT.VALID_PASSWORD_SPECIAL_CASE)
        }
        else if !VALID_NUMBER_CASE.evaluate(with: self)
        {
            return (false,CONSTANT.VALID_PASSWORD_NUMBERIC_CASE)
        }
        else if self.count < 8
        {
            return (false,CONSTANT.VALID_PASSWORD_MAXIMUM_CASE)
        }
        else
        {
            return (true, "")
        }
    }*/
    
}

extension NSString
{
    func isMaxmiumString(maxLength :Int, range :NSRange,string :String) -> Bool
    {
        let currentString: NSString = self
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func isMinimumString(minLength :Int, range :NSRange,string :String) -> Bool
    {
        let currentString: NSString = self
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= minLength
    }
}


extension CALayer
{
    func bottomAnimation(duration:CFTimeInterval)
    {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.duration = duration
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromTop
        self.add(animation, forKey: CATransitionType.push.rawValue)
    }
    
    func topAnimation(duration:CFTimeInterval)
    {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.duration = duration
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromBottom
        self.add(animation, forKey: CATransitionType.push.rawValue)
    }
}


extension UIView
{
    func pushTransition(_ duration:CFTimeInterval)
    {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromTop
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
}

/*
extension TransitionButton
{
    func userInteractionFalse()
    {
        self.isUserInteractionEnabled = false
        self.alpha = 0.5
    }
    
    func userInteractionTrue()
    {
        self.isUserInteractionEnabled = true
        self.alpha = 1
    }

}*/

extension UIButton
{
    func interactionFalse()
    {
        self.isUserInteractionEnabled = false
        self.alpha = 0.5
    }
    
    func interactionTrue()
    {
        self.isUserInteractionEnabled = true
        self.alpha = 1
    }
    
}



extension Int
{
    var degreesToRadians: Double
    {
        return Double(self) * .pi / 180
    }
}

extension FloatingPoint
{
    var degreesToRadians: Self
    {
        return self * .pi / 180
    }
    var radiansToDegrees: Self
    {
        return self * 180 / .pi
    }
}

extension UITableView
{
    func reloadWithAnimation()
    {
        self.reloadData()
        let tableViewHeight = self.bounds.size.height
        let cells = self.visibleCells
        var delayCounter = 0
        for cell in cells
        {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells
        {
            UIView.animate(withDuration: 1.6, delay: 0.08 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
            {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
    
    func reloadAnimation()
    {
        self.reloadData()
        var i = 0
        for cell in self.visibleCells
        {
            cell.transform = CGAffineTransform(translationX: 0, y: self.rowHeight/2)
            cell.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0.09 * Double(i) , options: [.curveEaseInOut], animations:
            {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
                    cell.alpha = 1
            }, completion: nil)
            i = i + 1
        }
    }
}

extension CLLocation
{
    public func bearingLocation(to destination: CLLocation) -> Double {
        // http://stackoverflow.com/questions/3925942/cllocation-category-for-calculating-bearing-w-haversine-function
        let lat1 = Double.pi * coordinate.latitude / 180.0
        let long1 = Double.pi * coordinate.longitude / 180.0
        let lat2 = Double.pi * destination.coordinate.latitude / 180.0
        let long2 = Double.pi * destination.coordinate.longitude / 180.0
        
        // Formula: θ = atan2( sin Δλ ⋅ cos φ2 , cos φ1 ⋅ sin φ2 − sin φ1 ⋅ cos φ2 ⋅ cos Δλ )
        // Source: http://www.movable-type.co.uk/scripts/latlong.html
        let rads = atan2(
            sin(long2 - long1) * cos(lat2),
            cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(long2 - long1))
        let degrees = rads * 180 / Double.pi
        
        return (degrees+360).truncatingRemainder(dividingBy: 360)
    }
}


/*

private var xoAssociationKey: CLLocation? = nil

extension GMSMarker
{
    var oldCoordinates: CLLocation!
    {
        get
        {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? CLLocation
        }
        set(newValue)
        {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }

}


private var xsAssociationKey: CLLocationCoordinate2D? = nil

extension GMSMarker
{
    var lastCoordinates: CLLocationCoordinate2D!
    {
        get
        {
            return objc_getAssociatedObject(self, &xsAssociationKey) as? CLLocationCoordinate2D
        }
        set(newValue)
        {
            objc_setAssociatedObject(self, &xsAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
}*/

extension UIImage
{
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ jpegQuality: JPEGQuality) -> Data?
    {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
    
    func resizeImage() -> UIImage?
    {
        var actualHeight: Float = Float(self.size.height)// Float(image.size.height)
        var actualWidth: Float = Float(self.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0, y: 0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = jpegData(compressionQuality: CGFloat(compressionQuality))
            
//            UIImageJPEGRepresentation(img!,CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
    
    func convertImageToBase64() -> String
    {
        let imageData = self.jpeg(.lowest)
        let base64String = imageData?.base64EncodedString()
        guard let data = base64String else
        {
            return ""
        }
        return data
    }
}

extension UIImageView
{
    func rotateClockwise()
    {
        UIView.animate(withDuration: 0.5)
        {
            () -> Void in
            self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
    }
    
    func rotateAnticlockwise()
    {
        
        UIView.animate(withDuration: 0.5)
        {
            () -> Void in
            self.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2)
        }
    }
}

extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor?
    {
        get
        {
            return self.placeHolderColor
        }
        set
        {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}


extension Date {
    
//    func daysBetween(date: Date) -> Int {
//        return Date.daysBetween(start: self, end: date)
//    }
    
    static func daysBetween(start: Date, end: Date) -> Int {
        
        
        
        
/*        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start).timeIntervalSinceReferenceDate
        let date2 = calendar.startOfDay(for: end).timeIntervalSinceReferenceDate*/
        
        let diff = start.timeIntervalSinceReferenceDate - end.timeIntervalSinceReferenceDate
        
//        print("diff = ",diff)
        
        return Int(diff)
        
        
//        let a = calendar.dateComponents([.second], from: date1, to: date2)
//        return a.value(for: .second)!
    }
}

/*
extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
*/


extension UIView
{
    func addDashedBorder()
    {
        let color = UIColor(named: "theme_color")
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color?.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        self.layer.addSublayer(shapeLayer)
    }
}

extension CGRect {
    var center: CGPoint { return CGPoint(x: midX, y: midY) }
}

extension UITableView
{
    func isCellVisible(section:Int, row: Int) -> Bool
    {
        guard let indexes = self.indexPathsForVisibleRows else
        {
            return false
        }
        return indexes.contains {$0.section == section && $0.row == row }
    }
}

extension UIImageView {

    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        self.layer.add(animation, forKey: nil)
    }

}

extension CLLocation {
    func geocode(completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void)  {
        CLGeocoder().reverseGeocodeLocation(self, completionHandler: completion)
    }
}

extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}

extension String
{
    var alphanumeric: String
    {
        return self.components(separatedBy: CharacterSet.alphanumerics.inverted).joined().lowercased()
    }
}

extension UIAlertController {
    
    //Set background color of UIAlertController
    func setBackgroundColor(color: UIColor) {
        if let bgView = self.view.subviews.first, let groupView = bgView.subviews.first, let contentView = groupView.subviews.first {
            contentView.backgroundColor = color
        }
    }
    
    //Set title font and title color
    func setTitlet(font: UIFont?, color: UIColor?) {
        guard let title = self.title else { return }
        let attributeString = NSMutableAttributedString(string: title)//1
        if let titleFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : titleFont],//2
                                          range: NSMakeRange(0, title.utf8.count))
        }
        
        if let titleColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor],//3
                                          range: NSMakeRange(0, title.utf8.count))
        }
        self.setValue(attributeString, forKey: "attributedTitle")//4
    }
    
    //Set message font and message color
    func setMessage(font: UIFont?, color: UIColor?) {
        guard let message = self.message else { return }
        let attributeString = NSMutableAttributedString(string: message)
        if let messageFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : messageFont],
                                          range: NSMakeRange(0, message.utf8.count))
        }
        
        if let messageColorColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : messageColorColor],
                                          range: NSMakeRange(0, message.utf8.count))
        }
        self.setValue(attributeString, forKey: "attributedMessage")
    }
    
    //Set tint color of UIAlertController
    func setTint(color: UIColor) {
        self.view.tintColor = color
    }
}
