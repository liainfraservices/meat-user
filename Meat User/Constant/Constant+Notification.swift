//
//  Constant+Notification.swift
//  GetTaxi
//
//  Created by Pyramidions on 06/02/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import Foundation

extension Notification.Name
{
    static let locationCheck = Notification.Name(rawValue: NotificationValue.LocationCheck.rawValue)
    static let logout = Notification.Name(rawValue: NotificationValue.Logout.rawValue)
    static let pushNotificationAlert = Notification.Name(rawValue: NotificationValue.pushNotification.rawValue)
    static let pushNotificationRedirection = Notification.Name(rawValue: NotificationValue.pushNotificationRedirection.rawValue)

}

enum NotificationValue: String
{
    case LocationCheck = "LocationCheck"
    case Logout = "Logout"
    case pushNotification = "pushNotification"
    case pushNotificationRedirection = "pushNotificationRedirection"
}
