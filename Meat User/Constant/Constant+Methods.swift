//
//  Constant+Methods.swift
//  FoodAppUser
//
//  Created by Pyramidions on 13/10/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications


func startLocalNotification(otp :String)
{
    let content = UNMutableNotificationContent()
    content.title = "OTP".localized()
    content.body = otp
    content.badge = 0
    
    //Setting time for notification trigger
    let date = Date(timeIntervalSinceNow: 1)
    let dateCompenents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
    
    let trigger = UNCalendarNotificationTrigger(dateMatching: dateCompenents, repeats: false)
    //Adding Request
    let request = UNNotificationRequest(identifier: "timerdone", content: content, trigger: trigger)
    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
}

func presentRight() -> CATransition
{
    let transition = CATransition()
    transition.duration = 0.6
    transition.type = CATransitionType.push
    transition.subtype = CATransitionSubtype.fromRight
    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    return transition
}

func presentLeft() -> CATransition
{
    let transition = CATransition()
    transition.duration = 0.6
    transition.type = CATransitionType.push
    transition.subtype = CATransitionSubtype.fromLeft
    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    return transition
}

func getDeviceID() ->String
{
    let deviceID = UIDevice.current.identifierForVendor!.uuidString
    return deviceID
}


func timeString(time:TimeInterval) -> String
{
    let minutes = Int(time) / 60 % 60
    let seconds = Int(time) % 60
    return String(format:"%2i:%02i",minutes, seconds)
}

/*

func saveUserData(data : RegisterData)
{
    print("saveUserData = ",data)
    
    UserDefaults.standard.setFirstName(value: data.firstName ?? "")
    UserDefaults.standard.setLastName(value: data.lastName ?? "")
    UserDefaults.standard.setMobileNo(value: data.mobile ?? "")
    UserDefaults.standard.setAccessToken(value: data.token ?? "")
    UserDefaults.standard.setUserImage(value: data.image ?? "")
    UserDefaults.standard.setRating(value: data.rating ?? 0.0)
    UserDefaults.standard.setLoggedIn(value: true)
}*/

func dateDifference(startDate : String,endDate : String) -> Int
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    let start = dateFormatter.date(from: startDate)!
    let end = dateFormatter.date(from: endDate)!
    
    let diff = Date.daysBetween(start: start, end: end) // 365    
    return diff
}

/*
func dateFormatChanger(fromDate : String) -> String
{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
//    dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatterGet.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"

//    dateFormatterGet.timeZone = NSTimeZone.local

    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd/MM/yyy, h:mm a"
    dateFormatterPrint.timeZone = TimeZone.current
    dateFormatterPrint.locale = Locale.current

    var output = ""
    
    if let date = dateFormatterGet.date(from: fromDate)
    {
        print(dateFormatterPrint.string(from: date))
        output = dateFormatterPrint.string(from: date)
    }
    else
    {
        print("There was an error decoding the string")
    }
    return output
}*/

func dateFormatChanger(fromDate : String) -> String
{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
//    dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatterGet.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"

//    dateFormatterGet.timeZone = NSTimeZone.local

    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd/MM/yyy"
    dateFormatterPrint.timeZone = TimeZone.current
    dateFormatterPrint.locale = Locale.current

    var output = ""
    
    if let date = dateFormatterGet.date(from: fromDate)
    {
        print(dateFormatterPrint.string(from: date))
        output = dateFormatterPrint.string(from: date)
    }
    else
    {
        print("There was an error decoding the string")
    }
    return output
}

func timeOnlyFormatChanger(fromDate : String) -> String
{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
//    dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatterGet.dateFormat = "HH:mm:ss"

    dateFormatterGet.timeZone = NSTimeZone.local
    dateFormatterGet.locale = Locale(identifier: "en_US")
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "H:mm a"
    dateFormatterPrint.timeZone = TimeZone.current
//    dateFormatterPrint.locale = Locale.current

    var output = ""
    
    if let date = dateFormatterGet.date(from: fromDate)
    {
        print(dateFormatterPrint.string(from: date))
        output = dateFormatterPrint.string(from: date)
    }
    else
    {
        print("There was an error decoding the string")
    }
    return output
}

func dateTimeFormatChanger(fromDate : String) -> String
{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
//    dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatterGet.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"

//    dateFormatterGet.timeZone = NSTimeZone.local

    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd/MM/yyy, h:mm a"
    dateFormatterPrint.timeZone = TimeZone.current
    dateFormatterPrint.locale = Locale.current

    var output = ""
    
    if let date = dateFormatterGet.date(from: fromDate)
    {
        print(dateFormatterPrint.string(from: date))
        output = dateFormatterPrint.string(from: date)
    }
    else
    {
        print("There was an error decoding the string")
    }
    return output
}


func localNotification(data: String)
{
    let content = UNMutableNotificationContent()
    content.title = CONSTANT.OOPS
//    content.subtitle = "from ioscreator.com"
    content.body = data
    /*
    // 2
    let imageName = "applelogo"
    guard let imageURL = Bundle.main.url(forResource: imageName, withExtension: "png") else { return }
        
    let attachment = try! UNNotificationAttachment(identifier: imageName, url: imageURL, options: .none)
        
    content.attachments = [attachment]*/
    
    // 3
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)
    // 4
    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)

}
