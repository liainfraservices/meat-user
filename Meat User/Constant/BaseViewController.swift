//
//  File.swift
//  GetTaxi
//
//  Created by Jai on 10/07/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import Foundation
import UIKit
import Lottie
import SwiftyJSON
import SwiftyGif

class BaseViewController: UIViewController
{
    
    let animationView = AnimationView()
    let greyView = UIView()

    var imageview = UIImageView() // Use -1 for infinite loop

    override func viewDidLoad()
    {
        super.viewDidLoad()
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
        
    override func viewWillAppear(_ animated: Bool)
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.enterForeground(notification:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
//        hideKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    @objc func enterForeground(notification: Notification)
    {
        print("Enter foreground")
        foregroundCalled()
    }
    

    
    func foregroundCalled()
    {
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func showLottie()
    {
        greyView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        greyView.backgroundColor = UIColor.black
        greyView.alpha = 0.5
        self.view.addSubview(greyView)
       
/*        let animation = Animation.named("white_loader")
        animationView.animation = animation
//        animationView.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        animationView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        animationView.center = self.view.center
        animationView.contentMode = .scaleAspectFill
        animationView.loopMode = .loop
        
//        animationView.layer.cornerRadius = 75
//        animationView.clipsToBounds = true
        
        view.addSubview(animationView)
        animationView.play()
        
        setNeedsStatusBarAppearanceUpdate()*/
        
        do
        {
            let gif = try UIImage(gifName: "Goat-loader.gif")
            self.imageview = UIImageView(gifImage: gif, loopCount: -1)
        }
        catch
        {
            print(error)
        }
              
        imageview.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        imageview.center = self.view.center
        imageview.contentMode = .scaleAspectFit
        view.addSubview(imageview)
        
        self.imageview.startAnimatingGif()


    }
    
    /*
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
           return .lightContent // .default
    }*/

    func hideLottie()
    {
            
            self.imageview.stopAnimatingGif()
    //        self.animationView.stop()
            self.greyView.removeFromSuperview()
            self.imageview.removeFromSuperview()
    //        self.animationView.removeFromSuperview()
            
            
            
    //        self.imageView.removeFromSuperview()
    }
    
    /*
    func showOffline()
    {
        let vc = OfflineViewController.instantiate(fromAppStoryboard: .Splash)
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }*/
    
    func hideKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func reloadOffline()
    {
        print("super foo")
    }
    
    func stopGesture()
    {
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    func logout()
    {
        let alert = UIAlertController(title: CONSTANT.OOPS, message: CONSTANT.UNAUTHORIZED,         preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: CONSTANT.OK,
                                      style: UIAlertAction.Style.default,
                                      handler:
            {
                (_: UIAlertAction!) in
                self.unauthorizedAccess()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func unauthorizedAccess()
    {
        UserDefaults.standard.clearUserDefaults()
        let vc = UINavigationController(rootViewController: SplashViewController.instantiate(fromAppStoryboard: .Main))
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.isNavigationBarHidden = true
        self.present(vc, animated: true, completion: nil)
    }

}
