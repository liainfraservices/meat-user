//
//  Singleton.swift
//  FoodAppUser
//
//  Created by Pyramidions on 06/12/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON

class ConstantManager
{
    static let appConstant = ConstantManager()
    
    var voipToken = ""

    var sideMenuSelectIndex = 0
    var branchID: String? = nil
    var branchName: String = ""

    var latitude = ""
    var longtitude = ""
    var completeAddress = ""

    var VAT = "0"
    
    func clear() -> Void
    {
        
    }
    
    var pushNotifyHandler: PushNotificationHandler = { }
    typealias PushNotificationHandler = () -> Void
    
    func alertNotify()
    {
        pushNotifyHandler()
    }
    

}
