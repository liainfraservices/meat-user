//
//  AWSHelper.swift
//  Hakeem
//
//  Created by Jai on 01/05/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import Foundation
import UIKit
//import AWSS3
//import AWSCore

/*
class AWSHelper
{
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?

    func uploadImage(chosenImage: UIImage,success:@escaping (String) -> Void, failure:@escaping (String) -> Void)
    {
        let compressedImage = chosenImage.jpeg(.medium)
        
        let imgData = compressedImage!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var strFileName = formatter.string(from: Date())
        strFileName = strFileName + ".png"
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock =
            {
                (task, progress) in DispatchQueue.main.async(execute:
                {
                    print("Image Upload = ",progress.fractionCompleted)
                    
                })
        }
        
        
        /*
        completionHandler =
            { (task, error) -> Void in
                DispatchQueue.main.async(execute:
                {
                    
                  
//
//
//                    if let error = task.error
//                    {
//                        print("Error: \(error.localizedDescription)")
//    //                    failure(error.localizedDescription)
//                    }
//
//                    if let _ = task.result
//                    {
//
//    //                    success(IMAGES_BUCKET + strFileName)
//
//                    }

                })
        }*/
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
                 completionHandler =
                  {
                      (task, error) -> Void in
                      DispatchQueue.main.async(execute:
                      {
                         if error == nil
                         {
                                 success(IMAGES_BUCKET + strFileName)
                         }
                         else
                         {
                            failure(error?.localizedDescription ?? "")
                         }
                     })
                 }
        
        
        let  transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadData(imgData,
                                   bucket: BUCKET_NAME,
                                   key: strFileName,
                                   contentType: "image/png",
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith
            {
                (task) -> AnyObject? in
                
                   if let error = task.error
                    {
                        print("Error: \(error.localizedDescription)")
                        
//                        DispatchQueue.main.async(execute:
//                        {
//                            failure(error.localizedDescription)
//                        })
                    }

                    if let _ = task.result
                    {

                        print("URL : \(IMAGES_BUCKET + strFileName)")
//                        DispatchQueue.main.async(execute:
//                        {
//                            success(IMAGES_BUCKET + strFileName)
//                        })

                    }
                return nil
        }
        
    }
}
*/
