//
//  APIList.swift
//  MVCDemo
//
//  Created by Pyramidions on 19/09/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import Foundation
import UIKit

struct APIList
{
        let BASE_URL = "https://www.yellostack.com/shaheapi/"
    
//        let SOCKET_URL = "http://service.matech.digital:3001/"
        func getUrlString(url: urlString) -> String
        {
            return BASE_URL + url.rawValue
        }
}

    
enum urlString: String
{
    case LOGIN = "user/customerlogin"
    case CREATE_ACCOUNT = "user/customersignup"
    case HOME = "user/Initialvalues"
    case PRODUCT_DETAILS = "user/getproductdetails"
    case LIST_CART = "user/listcart"
    case ADD_TO_CART = "user/addtocart"
    case LIST_ADDRESS = "user/listaddress/"
    case ADD_ADDRESS = "user/addaddress/"
    case DELETE_CART = "user/deletefromcart"
    case UPDATE_CART = "user/updatecart"
    case PLACE_ORDER = "user/placeorder"
    case LIST_ORDER = "user/listorder"
    case VIEWPROFILE = "user/viewprofile"
    case UPDATEPROFILE = "user/updateprofile"
    case CHANGEPASSWORD = "user/changepassword"
    case FORGOTPASSWORD = "user/forgotpassword"
    case DELETE_ADDDRESS = "user/deleteaddress"
    case LIST_BRANCHES = "user/branches"


    
    
    
    
    case UPDATE_DEVICE_TOKEN = "/patient/updateDeviceToken"
    case GETOTP = "/patient/getOTP"
    case CHECKOTP = "/patient/checkOTP"
    case NEW_PWD = "/patient/enterNewPassword"
    case FACEBOOK_LOGIN = "/patient/facebookLogin"
    case GOOGLE_LOGIN = "/patient/googleLogin"
    case APPLE_LOGIN = "/patient/appleLogin"
    case CHECKSOCIAL = "/patient/checkSocialLogin"
    case GET_PROFILE = "/patient/getMyProfile"
    case UPDATE_PROFILE = "/patient/updateProfile"
    case GETMYRECENTCONSULT = "/patient/getMyRecentConsultant"
    case GETMYCONSULTHISTORY = "/patient/getMyConsultationHistory"
    case DOCTOR_LIST = "/patient/listAvailableDoctors"
    case FEATURED_BOOKING = "/patient/featureBooking"
    case TRACK_PROVIDER = "/patient/trackProvider"
    case CANCEL_BOOKINGS = "/patient/cancelBooking"
    case APP_SETTINGS = "/patient/appSetting"
    case MAKE_PAYMENT = "/patient/pay"
    case ALL_DOCTOR_LIST = "/patient/getAllDoctors"
    case EMERGENCY_BOOKING = "/patient/newBooking"
    case POST_REVIEW = "/patient/postReview"
    case GET_ACTIVE_BOOKING = "/patient/getActiveBookings"
    case WALLET_HISTORY = "/patient/walletHistory"
    case ADD_MONEY_WALLET = "/patient/addMoneyWallet"
    case VIRTUAL_BOOKING = "/patient/virtualBooking"
    case PAY_FROM_WALLET = "/patient/payFromWallet"
    case COMPLETE_VIRTUAL_BOOKING = "/patient/completeVirtualBooking"

    

    case CREATECALL = "/patient/createCall"
    case ACCEPTCALL = "/patient/acceptCall"
    case ENDCALL = "/patient/endCall"
    case CHATDETAILLIST = "/patient/chatDetailList"
}

struct SocketList
{
    let BASE_URL = "http://52.91.243.240:3000"
    func getUrlString(url: socketString) -> String
    {
        return url.rawValue
    }
}

enum socketString: String
{
    case SEND_RANDOM_REQUEST = "sendRandomRequest"
    case UPDATE_PATIENT_LOCATION = "updatePatientLatLon"
    case LISTEN_PROVIDER = "provider_"
    
    case SEND_MESSAGE = "sendmessage"
    case RECEIVE_MESSAGE = "recievemessage"
    case GET_ONLINE = "get_online"

}

