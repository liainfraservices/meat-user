//
//  Connection.swift
//  MVCDemo
//
//  Created by Pyramidions on 19/09/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Connection
{
    func logout()
    {
        NotificationCenter.default.post(name: .logout, object: nil)
    }
    
    func requestPOSTRawData(_ url: String, params : String, headers : HTTPHeaders?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
    {
        
        print("URL = ",url)
        print("Parameter = ",params)
        
        if Connectivity.isConnectedToInternet()
        {
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "POST"
            request.setValue("text/plain", forHTTPHeaderField: "Content-Type")
            request.httpBody   = Data(params.utf8)
            Alamofire.request(request).responseJSON
            {
                    (responseObject) -> Void in
                    
                    print("Response = ",responseObject)
                    
                    if let json = responseObject.result.value
                    {
                        let jsonResponse = JSON(json)
                        print("JSON Response  = ",jsonResponse)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(LOADER_TIME), execute:
                    {
                        if responseObject.response?.statusCode == HTTPStatusCode.unauthorized.rawValue
                        {
                            self.logout()
                        }
                        else
                        {
                            switch responseObject.result
                            {
                                case .success:
                                
                                if let json = responseObject.result.value
                                {
                                    success(JSON(json))
                                }
                                
                                case .failure(let error):
                                    failure(error)
                            }
                        }
                })
            }
        }
        else
        {
//            let err = NSError(domain: "Check Internet Connection".localized(), code: nil, userInfo: nil)
            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.CHECK_INTERNET_CONNECTION])
            failure(error)
        }
    }

    func requestPOST(_ url: String, params : Parameters?, headers : HTTPHeaders?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
    {
        
        print("URL = ",url)
        print("Parameter = ",params!)
        
        if Connectivity.isConnectedToInternet()
        {
            if headers == nil
            {
                
                Alamofire.request(url, method: .post, parameters: params!,encoding: JSONEncoding.default, headers: nil).responseJSON
                {
                        (responseObject) -> Void in
                        
                        print("Response = ",responseObject)
                    
                        if let json = responseObject.result.value
                        {
                            let jsonResponse = JSON(json)
                            print("JSON Response  = ",jsonResponse)
                        }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(LOADER_TIME), execute:
                        {
                            if responseObject.response?.statusCode == HTTPStatusCode.unauthorized.rawValue
                            {
                                self.logout()
                            }
                            else
                            {
                                switch responseObject.result
                                {
                                    case .success:
                                    
                                    if let json = responseObject.result.value
                                    {
                                        success(JSON(json))
                                    }
                                    
                                    case .failure(let error):
                                        failure(error)
                                }
                            }
                    })

                    
                    
                        /* if responseObject.result.isSuccess {
                         let resJson = JSON(responseObject.result.value!)
                         success(resJson)
                         }
                         if responseObject.result.isFailure {
                         let error : Error = responseObject.result.error!
                         failure(error)
                         }*/
                }
            }
            else
            {
                
                print("Headers = ",headers!)
                
                Alamofire.request(url, method: .post, parameters: params!,encoding: JSONEncoding.default, headers: headers!).responseJSON
                    {
                        (responseObject) -> Void in
                        
                        print("Response = ",responseObject)
                        
                        if let json = responseObject.result.value
                        {
                            let jsonResponse = JSON(json)
                            print("JSON Response  = ",jsonResponse)
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(LOADER_TIME), execute:
                            {
                                if responseObject.response?.statusCode == HTTPStatusCode.unauthorized.rawValue
                                {
                                    self.logout()
                                }
                                else
                                {

                                switch responseObject.result
                                {
                                    case .success:
                                    
                                    if let json = responseObject.result.value
                                    {
                                        success(JSON(json))
                                    }
                                    
                                    case .failure(let error):
                                        failure(error)
                                }
                                }
                        })

                        /*                if responseObject.result.isSuccess {
                         let resJson = JSON(responseObject.result.value!)
                         success(resJson)
                         }
                         if responseObject.result.isFailure {
                         let error : Error = responseObject.result.error!
                         failure(error)
                         }*/
                }
            }
            
        }
        else
        {
//            let err = NSError(domain: "Check Internet Connection".localized(), code: nil, userInfo: nil)
            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.CHECK_INTERNET_CONNECTION])
            failure(error)
        }
    }
    
    
    func requestGET(_ url: String, params : Parameters?,headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
    {
        
        print("URL = ",url)
        print("Parameter = ",params)
        print("Headers = ",headers)

        
        if Connectivity.isConnectedToInternet()
        {
            do
            {
                Alamofire.request(url, method: .get, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON
                {
                    (response) in
                    
                    print("Response = ",response)
                    
                    if let json = response.result.value
                    {
                        let jsonResponse = JSON(json)
                        print("JSON Response  = ",jsonResponse)
                    }


                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(LOADER_TIME), execute:
                        {
                            if response.response?.statusCode == HTTPStatusCode.unauthorized.rawValue
                            {
                                self.logout()
                            }
                            else
                            {

                            switch response.result
                            {
                                case .success:
                                
                                if let json = response.result.value
                                {
                                    success(JSON(json))
                                }
                                
                                case .failure(let error):
                                    failure(error)
                            }
                            }
                    })
                }
            }
            catch let JSONError as NSError
            {
                failure(JSONError)
            }
        }
        else
        {
            //            let err = NSError(domain: "Check Internet Connection".localized(), code: nil, userInfo: nil)
            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.CHECK_INTERNET_CONNECTION])
            failure(error)
        }
        
    }
    
    func requestURLEncodingGET(_ url: String, params : Parameters?,headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
    {
        if Connectivity.isConnectedToInternet()
        {
            do
            {
                Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                    {
                        (response) in
                        
                        print("Response = ",response)
                        
                        if let json = response.result.value
                        {
                            let jsonResponse = JSON(json)
                            print("JSON Response  = ",jsonResponse)
                        }


                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(LOADER_TIME), execute:
                            {
                                if response.response?.statusCode == HTTPStatusCode.unauthorized.rawValue
                                {
                                    self.logout()
                                }
                                else
                                {

                                switch response.result
                                {
                                    case .success:
                                    
                                    if let json = response.result.value
                                    {
                                        success(JSON(json))
                                    }
                                    
                                    case .failure(let error):
                                        failure(error)
                                }
                                }
                        })
                        
                }
            }
            catch let JSONError as NSError
            {
                failure(JSONError)
            }
        }
        else
        {
            //            let err = NSError(domain: "Check Internet Connection".localized(), code: nil, userInfo: nil)
            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.CHECK_INTERNET_CONNECTION])
            failure(error)
        }
        
    }
    
    func uploadImage(_ url: String, params :Data,headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
    {
            Alamofire.upload(multipartFormData:
            {
                (multipartFormData) in
                multipartFormData.append(params, withName: "file", fileName: "image.png", mimeType: "image/png")
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: nil, encodingCompletion:
            {
                (result) in
                
                switch result
                {
                    case .success(let upload, _, _):
                    upload.responseJSON
                    {
                        response in
//                        print("Upload Image = ",response)
                        

                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(LOADER_TIME), execute:
                            {
                                if response.response?.statusCode == HTTPStatusCode.unauthorized.rawValue
                                {
                                    self.logout()
                                }
                                else
                                {

                                switch response.result
                                {
                                    case .success:
                                    
                                    if let json = response.result.value
                                    {
                                        success(JSON(json))
                                    }
                                    
                                    case .failure(let error):
                                        failure(error)
                                }
                                }
                        })
                        /*
                        
                        let jsonResponse = JSON(response.result.value)
                        
                        if let err = response.error
                        {
//                            self.showAlert(title: "Oops".localized(), msg: jsonResponse["error_message"].stringValue)
//                            print(err)
//                            return
                        }
                        else
                        {
//                            self.imageName = jsonResponse["image"].stringValue
//                            print("Succesfully uploaded")
//                            self.setProfile()
                            
                        }*/
                    }
                    case .failure(let error):
                        failure(error)
//                        print("Error in upload: \(error.localizedDescription)")
                }
            })
    }
    
    func requestJSON(_ url: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
    {
        
        print("URL = ",url)
        
        Alamofire.request(url).responseJSON
            {
                response in

                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(LOADER_TIME), execute:
                    {
                        if response.response?.statusCode == HTTPStatusCode.unauthorized.rawValue
                        {
                            self.logout()
                        }
                        else
                        {

                        switch response.result
                        {
                            case .success:
                            
                            if let json = response.result.value
                            {
                                success(JSON(json))
                            }
                            
                            case .failure(let error):
                                failure(error)
                        }
                        }
                })
                
        }
    }
    
    
}

class Connections
{
    func requestPOST(_ url: String, params : Parameters?, headers : HTTPHeaders?, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void)
    {
        
        print("URL = ",url)
        print("Parameter = ",params!)
        
        if Connectivity.isConnectedToInternet()
        {
            if headers == nil
            {
                
                Alamofire.request(url, method: .post, parameters: params!, encoding: URLEncoding.httpBody, headers: nil).responseJSON
                {
                        (responseObject) -> Void in
                        
                        print("Response = ",responseObject)
                        
                        switch responseObject.result
                        {
                            case .success:
                                if let data = responseObject.data
                                {
                                   success(data)
                                }
                            case .failure(let error):
                                failure(error)
                        }
                        /* if responseObject.result.isSuccess {
                         let resJson = JSON(responseObject.result.value!)
                         success(resJson)
                         }
                         if responseObject.result.isFailure {
                         let error : Error = responseObject.result.error!
                         failure(error)
                         }*/
                }
            }
            else
            {
                
                print("Headers = ",headers!)

                Alamofire.request(url, method: .post, parameters: params!, encoding: URLEncoding.httpBody, headers: headers!).responseJSON
                    {
                        (responseObject) -> Void in
                        
                        print("Response = ",responseObject)
                        
                        switch responseObject.result
                        {
                            case .success:
                                if let data = responseObject.data
                                {
                                    success(data)
                                }
                            case .failure(let error):
                                failure(error)
                        }
                        
                        /*                if responseObject.result.isSuccess {
                         let resJson = JSON(responseObject.result.value!)
                         success(resJson)
                         }
                         if responseObject.result.isFailure {
                         let error : Error = responseObject.result.error!
                         failure(error)
                         }*/
                }
            }
            
        }
        else
        {
//            let err = NSError(domain: "Check Internet Connection".localized(), code: nil, userInfo: nil)
            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.CHECK_INTERNET_CONNECTION])
            failure(error)
        }
    }
    
    
    func requestGET(_ url: String, params : Parameters?,headers : [String : String]?, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void)
    {
        
        print("URL = ",url)
        print("Parameter = ",params)
        print("Headers = ",headers)

        
        if Connectivity.isConnectedToInternet()
        {
            
            if params == nil
            {
                do
                {
                    Alamofire.request(url, method: .get, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON
                    {
                        (response) in
                        
                        print("Response = ",response)

                        switch response.result
                        {
                            case .success:
                                if let data = response.data
                                {
                                    success(data)
                                }
                            case .failure(let error):
                                failure(error)
                        }
                    }
                }
                catch let JSONError as NSError
                {
                    failure(JSONError)
                }
            }
            else
            {
                do
                {
                    Alamofire.request(url, method: .get, parameters: params!, headers: headers!).responseJSON
                        {
                            (response) in
                            
                            print("Response = ",response)
                            
                            switch response.result
                            {
                            case .success:
                                if let data = response.data
                                {
                                    success(data)
                                }
                            case .failure(let error):
                                failure(error)
                            }
                    }
                }
                catch let JSONError as NSError
                {
                    failure(JSONError)
                }
            }
        }
        else
        {
            //            let err = NSError(domain: "Check Internet Connection".localized(), code: nil, userInfo: nil)
            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.CHECK_INTERNET_CONNECTION])
            failure(error)
        }
        
    }
    
    func requestURLEncodingGET(_ url: String, params : Parameters?,headers : [String : String]?, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void)
    {
        if Connectivity.isConnectedToInternet()
        {
            do
            {
                Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                    {
                        (response) in
                        
                        print("Response = ",response)
                        
                        switch response.result
                        {
                        case .success:
                            if let data = response.data
                            {
                                success(data)
                            }
                        case .failure(let error):
                            failure(error)
                        }
                }
            }
            catch let JSONError as NSError
            {
                failure(JSONError)
            }
        }
        else
        {
            //            let err = NSError(domain: "Check Internet Connection".localized(), code: nil, userInfo: nil)
            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.CHECK_INTERNET_CONNECTION])
            failure(error)
        }
        
    }
    
    func uploadImage(_ url: String, params :Data,headers : [String : String]?, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void)
    {
            Alamofire.upload(multipartFormData:
            {
                (multipartFormData) in
                multipartFormData.append(params, withName: "file", fileName: "image.png", mimeType: "image/png")
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: nil, encodingCompletion:
            {
                (result) in
                

                switch result
                {
                    case .success(let upload, _, _):
                    upload.responseJSON
                    {
                        response in
//                        print("Upload Image = ",response)
                        
                        print("Upload Image = ",result)

                        switch response.result
                        {
                            case .success:
                                if let data = response.data
                                {
                                    success(data)
                                }
                            case .failure(let error):
                                failure(error)
                        }
                        
                        /*
                        
                        let jsonResponse = JSON(response.result.value)
                        
                        if let err = response.error
                        {
//                            self.showAlert(title: "Oops".localized(), msg: jsonResponse["error_message"].stringValue)
//                            print(err)
//                            return
                        }
                        else
                        {
//                            self.imageName = jsonResponse["image"].stringValue
//                            print("Succesfully uploaded")
//                            self.setProfile()
                            
                        }*/
                    }
                    case .failure(let error):
                        failure(error)
//                        print("Error in upload: \(error.localizedDescription)")
                }
            })
    }
    
    func requestJSON(_ url: String, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void)
    {
        
        print("URL = ",url)
        
        Alamofire.request(url).responseJSON
            {
                response in
                switch response.result
                {
                case .success:
                    if let data = response.data
                    {
                        success(data)
                    }
                case .failure(let error):
                    failure(error)
                    break
                }
        }
    }
    
    
}

class NetworkManager: NSObject
{
    var reachability: Reachability!
    
    static let sharedInstance: NetworkManager = { return NetworkManager() }()
    
    override init() {
        super.init()
        
        reachability = Reachability()!
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
    }
    
    static func stopNotifier() -> Void {
        do {
            try (NetworkManager.sharedInstance.reachability).startNotifier()
        } catch {
            print("Error stopping notifier")
        }
    }
    
    static func isReachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection != .none {
            completed(NetworkManager.sharedInstance)
        }
    }
    
    static func isUnreachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .none {
            completed(NetworkManager.sharedInstance)
        }
    }
    
    static func isReachableViaWWAN(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .cellular {
            completed(NetworkManager.sharedInstance)
        }
    }
    
    static func isReachableViaWiFi(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .wifi {
            completed(NetworkManager.sharedInstance)
        }
    }
}


