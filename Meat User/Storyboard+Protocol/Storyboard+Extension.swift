//
//  Storyboard+Extension.swift
//  Docnow
//
//  Created by Pyramidions on 25/07/19.
//  Copyright © 2019 Jai. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController
{
    class var storyboardID : String
    {
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
