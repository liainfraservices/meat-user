//
//  Storyboard+Enum.swift
//  Docnow
//
//  Created by Pyramidions on 25/07/19.
//  Copyright © 2019 Jai. All rights reserved.
//

import Foundation
import UIKit

enum AppStoryboard : String
{
    
    case Splash
    case Main
    case Home
    case Profile
    case Wallet
    case Bookings
    case Patient
    case Payment
    case Chat
    case Address
    case Order
    
    var instance: UIStoryboard
    {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type) -> T
    {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else
        {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard")
        }
        
        return scene
    }
    
}

