//
//  File.swift
//  GetTaxi
//
//  Created by Pyramidions on 23/02/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AddressSearchServiceLayer: NSObject
{
    let sharedInstance = Connections()
    
    var address = ""
    var key = ""
    let url = "https://maps.googleapis.com/maps/api/geocode/json"
    var lat = ""
    var log = ""
    
    func requestAddress(success:@escaping (AddressSearchModal) -> Void,failure:@escaping (Error) -> Void)
    {
        let parameter:[String: Any] = [ "address": address,"key":key]
        
        sharedInstance.requestURLEncodingGET(url, params: parameter, headers: nil, success:
        {
                (JSON) in
                let  result :Data? = JSON
                if result != nil
                {
                    do
                    {
                        let response = try JSONDecoder().decode(AddressSearchModal.self, from: result!)
                        if response.status == "OK"
                        {
                                success(response)
                        }
                        else
                        {
                            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : response.status ?? ""])
                            failure(error)
                        }
                    }
                    catch let error as NSError
                    {
                        failure(error)
                    }
                }
                else
                {
                    let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.INVALID_REQUEST])
                    failure(error)
                }
        },
        failure:
        {
                (error) in
                failure(error)
        })
    }
    
    func geocodingRequest(success:@escaping (ReverseGeocodingModal) -> Void,failure:@escaping (Error) -> Void)
    {
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(log)&key=\(key)"
  
        print("URL = ",url)
        
        
//        https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=AIzaSyCt3rNL6-8e4LUIqS6av1Jdhur7NjdLtas
        
        sharedInstance.requestURLEncodingGET(url, params: nil, headers: nil, success:
            {
                (JSON) in
                let  result :Data? = JSON
                if result != nil
                {
                    do
                    {
                        let response = try JSONDecoder().decode(ReverseGeocodingModal.self, from: result!)
                        if response.status == "OK"
                        {
                            success(response)
                        }
                        else
                        {
                            let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : response.status ?? ""])
                            failure(error)
                        }
                    }
                    catch let error as NSError
                    {
                        failure(error)
                    }
                }
                else
                {
                    let error = NSError(domain: "", code: 4, userInfo: [NSLocalizedDescriptionKey : CONSTANT.INVALID_REQUEST])
                    failure(error)
                }
        },
                                             failure:
            {
                (error) in
                failure(error)
        })
    }
    
    
}



