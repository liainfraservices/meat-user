//
//  File.swift
//  GetTaxi
//
//  Created by Pyramidions on 06/02/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import Foundation
import Alamofire

protocol AddressSearchViewModalDelegate: class
{
    func didRecieveBookingServiceDataUpdate(data : AddressSearchModal,result :SearchResultAddressModel)
    func didFailBookingServiceError(error: Error)
}

protocol GeocodingRequestViewModalDelegate: class
{
    func didRecieveGeocodingRequestUpdate(data: ReverseGeocodingModal)
    func didFailGeocodingRequestError(error: Error)
}


class AddressSearchViewModal
{
    weak var geocodingDelegate: GeocodingRequestViewModalDelegate?
    weak var delegate: AddressSearchViewModalDelegate?
    private let dataSource = AddressSearchServiceLayer()
    var address = ""
    var key = ""
    var searchResult : SearchResultAddressModel?

    func requestData() -> Void
    {
        dataSource.address = address
        dataSource.key = key
        dataSource.requestAddress(success:
        {
            (data) in
            self.delegate?.didRecieveBookingServiceDataUpdate(data: data, result: self.searchResult!)
        },
        failure:
        {
            (error) in
            self.delegate?.didFailBookingServiceError(error: error)
        })
    }
    
    func geocodingRequest(lat: String, log: String) -> Void
    {
        dataSource.lat = lat
        dataSource.log = log
        dataSource.key = MAP_KEY
        dataSource.geocodingRequest(success:
        {
                (data) in
                self.geocodingDelegate?.didRecieveGeocodingRequestUpdate(data: data)
        },
        failure:
        {
                (error) in
                self.geocodingDelegate?.didFailGeocodingRequestError(error: error)
        })
    }
    
}
