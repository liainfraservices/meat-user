//
//  AddressSearchViewController.swift
//  GetTaxi
//
//  Created by Pyramidions on 05/02/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire
import SwiftyJSON
//import Toast_Swift

protocol AddressSearchViewControllerDelegate: class
{
    func didSelectLocation(place: String,address : String, latitude : String, longitude : String, location: AddressSearchLocation?)
}

class AddressSearchViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,GMSAutocompleteFetcherDelegate
{
    
    
//    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var mapTopView: UIView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var enableSearchBarBtn: UIButton!
    @IBOutlet weak var resultView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()

    var isFetchLocation = false
    
    var currentLocation: CLLocation? = nil
    var dataSource = AddressSearchViewModal()
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var resultsArray: [NSDictionary] = []
    var searchArray = [SearchResultAddressModel]()
    var gmsFetcher: GMSAutocompleteFetcher!
    let sharedInstance = Connection()
    var isSearch : Bool = false
    weak var delegate : AddressSearchViewControllerDelegate?
    var isSource = true
    var searchResultArray = [SearchResultAddressModel]()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        searchBar.semanticContentAttribute = .forceRightToLeft        
        
        confirmBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        confirmBtn.setTitle(CONSTANT.CONFIRM_LOCATION, for: .normal)
        
        enableSearchBarBtn.isHidden = true
//        searchResultArray = UserDefaults.standard.getToAddress() ?? []

        tableView.register(UINib(nibName: "SearchMapCell", bundle: nil), forCellReuseIdentifier: "SearchMapCell")
        
        let headerNib = UINib.init(nibName: "SearchMapHeader", bundle: Bundle.main)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "SearchMapHeader")

        dataSource.delegate = self
        locationManager.delegate = self
        
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self

//        if currentLocation != nil
//        {
//            let bounds = GMSCoordinateBounds(location: (currentLocation?.coordinate)!, coordinateters: 500)
//            gmsFetcher.coordinateBounds = bounds
//        }
        
        
//        titleLbl.font = UIFont.appBoldFontWith(size: 18)
//        titleLbl.text = CONSTANT.PICK_UP_LOCATION
        searchBar.placeholder = CONSTANT.SERACHBAR_PLACEHOLDER
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.appMediumFontWith(size: 14)

        tableView.keyboardDismissMode = .onDrag
        searchBar.becomeFirstResponder()
        
        self.mapView.settings.rotateGestures = false;
        self.mapView.settings.tiltGestures = false        
        
        /*
        do
        {
            if let styleURL = Bundle.main.url(forResource: "google_map_style", withExtension: "json")
            {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            }
            else
            {
                NSLog("Unable to find style.json")
            }
        }
        catch
        {
            NSLog("One or more of the map styles failed to load. \(error)")
        }*/
        
        self.dataSource.geocodingDelegate = self
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isSearch
        {
            return searchArray.count
        }
        else
        {
            return searchResultArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchMapCell", for: indexPath) as! SearchMapCell
        
        cell.seperatorLbl.isHidden = true
        
        if isSearch
        {
            if searchArray.count > 0
            {
                let dict = searchArray[indexPath.row]
                cell.locationLbl.text = dict.place ?? ""
                cell.detailLbl.text = dict.complete ?? ""
                cell.selectionStyle = .none
                if (searchArray.count - 1) ==  indexPath.row
                {
                    cell.seperatorLbl.isHidden = true
                }
            }
        }
        else
        {
            if searchResultArray.count > 0
            {
                let dict = searchResultArray[indexPath.row]
                cell.locationLbl.text = dict.place ?? ""
                cell.detailLbl.text = dict.complete ?? ""
                cell.selectionStyle = .none
                if (searchResultArray.count - 1) ==  indexPath.row
                {
                    cell.seperatorLbl.isHidden = true
                }
            }
        }
    
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if isSearch
        {
            if searchArray.count > 0
            {
                /*
                let dict = searchArray[indexPath.row]                
//                saveRecentResult(dict: dict)
                searchBar.resignFirstResponder()
                getAddress(address: dict.complete ?? "", addressModal: dict)*/
                
                let dict = searchArray[indexPath.row]
                
                let placeClient = GMSPlacesClient()

                placeClient.lookUpPlaceID(dict.placeID ?? "") { (place, error) -> Void in
                    if let error = error {
                         //show error
                        return
                    }

                    if let place = place
                    {
                         //longitude
                         //latitude
                        
                        self.chooseFromMap(lat: place.coordinate.latitude, long: place.coordinate.longitude)
                    }
                    else
                    {
                        //show error
                    }
                }
                
            }
        }
        else
        {
            if searchResultArray.count > 0
            {
                /*
                let dict = searchResultArray[indexPath.row]
                searchBar.resignFirstResponder()
                getAddress(address: dict.complete ?? "", addressModal: dict)*/
                
                let dict = searchResultArray[indexPath.row]

                let placeClient = GMSPlacesClient()

                placeClient.lookUpPlaceID(dict.placeID ?? "") { (place, error) -> Void in
                    if let error = error {
                         //show error
                        return
                    }

                    if let place = place
                    {
                         //longitude
                         //latitude
                        
                        self.chooseFromMap(lat: place.coordinate.latitude, long: place.coordinate.longitude)
                    }
                    else
                    {
                        //show error
                    }
                }

            }
        }
    }
    
    
    func chooseFromMap(lat: Double, long: Double)
    {
            searchBar.resignFirstResponder()
            enableSearchBarBtn.isHidden = false

            if self.mapView.delegate == nil
            {
                self.mapView.delegate = self
                
//                if !isFetchLocation
//                {
//                    isFetchLocation = true
//
//                    var locate: CLLocation?
                    
                    mapView.moveCamera(GMSCameraUpdate.setTarget(CLLocationCoordinate2D(latitude: (lat), longitude: (long)), zoom: 16))

//
//                    let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 4)
//                    self.mapView.camera = camera

                    
                    /*
                     
                    /*
                    
                    if isSource
                    {
                        self.dataSource.geocodingRequest(lat: String(describing: ConstantManager.appConstant.sourceLoc?.coordinate.latitude ?? 0.0), log: String(describing: ConstantManager.appConstant.sourceLoc?.coordinate.longitude ?? 0.0))

                        locate = ConstantManager.appConstant.sourceLoc
                    }
                    else
                    {
                        self.dataSource.geocodingRequest(lat: String(describing: ConstantManager.appConstant.destinationLoc?.coordinate.latitude ?? 0.0), log: String(describing: ConstantManager.appConstant.destinationLoc?.coordinate.longitude ?? 0.0))
                        
                        locate = ConstantManager.appConstant.destinationLoc

                    }*/
                    
                    if locate != nil
                    {
                        self.dataSource.geocodingRequest(lat: String(describing: locate!.coordinate.latitude), log: String(describing: locate!.coordinate.longitude))
                    }
                    else
                    {
                        self.dataSource.geocodingRequest(lat: String(describing: currentLocation?.coordinate.latitude ?? 0.0), log: String(describing: currentLocation?.coordinate.longitude ?? 0.0))
                    }*/

                    
    //                self.dataSource.geocodingRequest(lat: String(describing: currentLocation!.coordinate.latitude), log: String(describing: currentLocation!.coordinate.longitude))
//                }
    //            self.getAdressName(coords: currentLocation!)
            }
            else
            {
                mapView.moveCamera(GMSCameraUpdate.setTarget(CLLocationCoordinate2D(latitude: (lat), longitude: (long)), zoom: 16))
            }
            resultView.hideShow(hidden: true)
        }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 67
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 68
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SearchMapHeader") as! SearchMapHeader
        
        headerView.currentLocationBtn.addTarget(self, action: #selector(AddressSearchViewController.pinLocation(sender:)), for: .touchUpInside)
        
        return headerView
    }
    
    @objc func pinLocation(sender: UIButton)
    {
        searchBar.resignFirstResponder()
        enableSearchBarBtn.isHidden = false

        if self.mapView.delegate == nil
        {
            self.mapView.delegate = self
            
            if !isFetchLocation
            {
                isFetchLocation = true
                
                var locate: CLLocation?
                
                /*
                
                if isSource
                {
                    self.dataSource.geocodingRequest(lat: String(describing: ConstantManager.appConstant.sourceLoc?.coordinate.latitude ?? 0.0), log: String(describing: ConstantManager.appConstant.sourceLoc?.coordinate.longitude ?? 0.0))

                    locate = ConstantManager.appConstant.sourceLoc
                }
                else
                {
                    self.dataSource.geocodingRequest(lat: String(describing: ConstantManager.appConstant.destinationLoc?.coordinate.latitude ?? 0.0), log: String(describing: ConstantManager.appConstant.destinationLoc?.coordinate.longitude ?? 0.0))
                    
                    locate = ConstantManager.appConstant.destinationLoc

                }*/
                
                if locate != nil
                {
                    self.dataSource.geocodingRequest(lat: String(describing: locate!.coordinate.latitude), log: String(describing: locate!.coordinate.longitude))
                }
                else
                {
                    self.dataSource.geocodingRequest(lat: String(describing: currentLocation?.coordinate.latitude ?? 0.0), log: String(describing: currentLocation?.coordinate.longitude ?? 0.0))
                }

                
//                self.dataSource.geocodingRequest(lat: String(describing: currentLocation!.coordinate.latitude), log: String(describing: currentLocation!.coordinate.longitude))
            }
//            self.getAdressName(coords: currentLocation!)
        }
        resultView.hideShow(hidden: true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        isSearch = true
        self.searchArray.removeAll()
        gmsFetcher?.sourceTextHasChanged(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        isSearch = false
        searchBar.resignFirstResponder()
    }
    
    public func didFailAutocompleteWithError(_ error: Error)
    {
        isSearch = false
        self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
    }
    
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction])
    {
        print("predictions = ",predictions)
        
        if predictions.count > 0
        {
            for prediction in predictions
            {
                if let prediction = prediction as GMSAutocompletePrediction?
                {
                    
                    searchArray.append(SearchResultAddressModel.init(place: prediction.attributedPrimaryText.string, placeID: prediction.placeID, complete: prediction.attributedFullText.string))
                }
            }
        }
        else
        {
            if !(searchBar.text!.isEmpty)
            {
//                self.view.endEditing(true)
//                self.view.makeToast(CONSTANT.VALID_ADDRESS, duration: 1.0, position: .top)
//                self.view.makeToast("Enter valid address")
            }
            isSearch = false
        }        
        tableView.reloadData()
    }
    
    func showOffline()
    {
    }
    
    @IBAction func hideMapViewAtn(_ sender: Any)
    {
        resultView.hideShow(hidden: false)
        enableSearchBarBtn.isHidden = true
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func confirmAtn(_ sender: Any)
    {
        let dict = SearchResultAddressModel.init(place: searchBar.text!, placeID: "", complete: searchBar.text!)
//        saveRecentResult(dict: dict)
        searchBar.resignFirstResponder()
        getAddress(address: dict.complete ?? "", addressModal: dict)
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension AddressSearchViewController
{
    
    
    func getAddress(address:String, addressModal : SearchResultAddressModel)
    {
        dataSource.searchResult = addressModal
        dataSource.key = MAP_KEY
        dataSource.address = address
        dataSource.requestData()
    }
}


extension AddressSearchViewController : AddressSearchViewModalDelegate
{
    func didRecieveBookingServiceDataUpdate(data: AddressSearchModal, result: SearchResultAddressModel)
    {
        self.navigationController?.popViewController(animated: true)

        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        {
            if (data.results?.count)! > 0
            {
                
                self.delegate?.didSelectLocation(place: result.place ?? "", address: result.complete ?? "", latitude: String(describing:  data.results?[0].geometry?.location?.lat! ?? 0.0), longitude: String(describing:  data.results?[0].geometry?.location?.lng! ?? 0.0), location: data.results?[0].geometry?.location)

            }
        }
    }
    
    func didFailBookingServiceError(error: Error)
    {
        if error.localizedDescription == CONSTANT.CHECK_INTERNET_CONNECTION
        {
            self.showOffline()
        }
        else
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}

extension AddressSearchViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    {
        if !isFetchLocation
        {
            isFetchLocation = true
            self.dataSource.geocodingRequest(lat: String(describing: position.target.latitude), log: String(describing: position.target.longitude))
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        
    }
}

extension AddressSearchViewController: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = locations.last else
        {
            return
        }
        
        currentLocation = location
        
        /*
        if isSource
        {
            if ConstantManager.appConstant.sourceLoc != nil
            {
                let camera = GMSCameraPosition.camera(withLatitude: (ConstantManager.appConstant.sourceLoc!.coordinate.latitude), longitude: (ConstantManager.appConstant.sourceLoc!.coordinate.longitude), zoom: 17.0)
                self.mapView?.animate(to: camera)
            }
            else
            {
                let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 17.0)
                self.mapView?.animate(to: camera)
            }
        }
        else
        {
            if ConstantManager.appConstant.destinationLoc != nil
            {
                let camera = GMSCameraPosition.camera(withLatitude: (ConstantManager.appConstant.destinationLoc!.coordinate.latitude), longitude: (ConstantManager.appConstant.destinationLoc!.coordinate.longitude), zoom: 17.0)
                self.mapView?.animate(to: camera)
            }
            else
            {
                let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 17.0)
                self.mapView?.animate(to: camera)
            }
        }*/
        
        let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 17.0)
        self.mapView?.animate(to: camera)

        
        self.locationManager.stopUpdatingLocation()
    }
    
}

extension AddressSearchViewController: GeocodingRequestViewModalDelegate
{
    func didRecieveGeocodingRequestUpdate(data: ReverseGeocodingModal)
    {
        isFetchLocation = false
        if (data.results?.count ?? 0) > 0
        {
            self.searchBar.text = data.results?[0].formattedAddress ?? ""
        }
    }
    
    func didFailGeocodingRequestError(error: Error)
    {
        print("Error = ",error.localizedDescription)
        isFetchLocation = false
//        self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
    }
}
