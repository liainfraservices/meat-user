//
//  ListAddressViewController.swift
//  Meat User
//
//  Created by Jai on 10/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class AddNewAddressViewController: BaseViewController
{

    @IBOutlet weak var tableView: UITableView!
    var dataArray: [JSON] = []
    let connection = Connection()
    
    var cartArray: [JSON] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView?.register(AddAddressTableViewCell.nib, forCellReuseIdentifier: AddAddressTableViewCell.identifier)
        tableView?.register(ListBranchTableViewCell.nib, forCellReuseIdentifier: ListBranchTableViewCell.identifier)
        tableView.contentInset = UIEdgeInsets.init(top: 20, left: 0, bottom: 0, right: 0)
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        getList()
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

}

extension AddNewAddressViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.dataArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == self.dataArray.count
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddAddressTableViewCell.identifier, for: indexPath) as! AddAddressTableViewCell
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let item = dataArray[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: ListBranchTableViewCell.identifier, for: indexPath) as! ListBranchTableViewCell
            cell.branchNameLbl.text = item["address"].stringValue
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.dataArray.count
        {
            let vc = DeliveryAddressViewController.instantiate(fromAppStoryboard: .Home)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {}
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        
        let delete = UITableViewRowAction(style: .default, title: CONSTANT.DELETE)
        {
            (action, indexPath) in
            self.tableView.dataSource?.tableView!(self.tableView, commit: .delete, forRowAt: indexPath)
            return
        }
        
        delete.backgroundColor = UIColor.init(named: "theme_color") ?? UIColor.green

        return [delete]


        /*
        if editingStyle == .delete
        {
            self.deleteAddress(addressid: self.dataArray[indexPath.row]["id"].stringValue)
            self.dataArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }*/
    }

    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            self.deleteAddress(addressid: self.dataArray[indexPath.row]["id"].stringValue)
            self.dataArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

}

extension AddNewAddressViewController
{
        
    func deleteAddress(addressid: String)
    {
        let url =  APIList().getUrlString(url: .DELETE_ADDDRESS)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "addressid": addressid
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.tableView.reloadData()
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func getList()
    {
        let url =  APIList().getUrlString(url: .LIST_ADDRESS)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userId": UserDefaults.standard.getUserId()
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.dataArray = data["data"].arrayValue
                self.tableView.reloadData()
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}

