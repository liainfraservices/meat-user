//
//  ListBranchTableViewCell.swift
//  Meat User
//
//  Created by Jai on 09/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

class AddAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var branchNameLbl: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        branchNameLbl.font = UIFont.appMediumFontWith(size: 14)
        branchNameLbl.text = CONSTANT.ADD_ADDRESS
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class var identifier: String
    {
        return String(describing: self)
    }
      
    class var nib: UINib
    {
        return UINib(nibName: identifier, bundle: nil)
    }

    
}
