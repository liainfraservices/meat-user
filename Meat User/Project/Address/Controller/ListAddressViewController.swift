//
//  ListAddressViewController.swift
//  Meat User
//
//  Created by Jai on 10/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ListAddressViewController: BaseViewController
{

    @IBOutlet weak var tableView: UITableView!
    var dataArray: [JSON] = []
    let connection = Connection()
    
    var cartArray: [JSON] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView?.register(AddAddressTableViewCell.nib, forCellReuseIdentifier: AddAddressTableViewCell.identifier)
        tableView?.register(ListBranchTableViewCell.nib, forCellReuseIdentifier: ListBranchTableViewCell.identifier)
        tableView.contentInset = UIEdgeInsets.init(top: 20, left: 0, bottom: 0, right: 0)    
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        getList()
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ListAddressViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.dataArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == self.dataArray.count
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddAddressTableViewCell.identifier, for: indexPath) as! AddAddressTableViewCell
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let item = dataArray[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: ListBranchTableViewCell.identifier, for: indexPath) as! ListBranchTableViewCell
            cell.delegate = self
            cell.branchNameLbl.text = item["address"].stringValue
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.dataArray.count
        {
            let vc = DeliveryAddressViewController.instantiate(fromAppStoryboard: .Home)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let item = dataArray[indexPath.row]
            let vc = BeforeOrderPlaceListViewController.instantiate(fromAppStoryboard: .Address)
            vc.dataArray = self.cartArray
            vc.addressid = item["id"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}

extension ListAddressViewController
{
    func getList()
    {
        let url =  APIList().getUrlString(url: .LIST_ADDRESS)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userId": UserDefaults.standard.getUserId()
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.dataArray = data["data"].arrayValue
                self.tableView.reloadData()
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}

extension ListAddressViewController: ListBranchTableViewDelegate
{
    func didSelectListBranch(cell: ListBranchTableViewCell)
    {
        if let indexPath = tableView.indexPath(for: cell)
        {
            if indexPath.row == self.dataArray.count
            {
                let vc = DeliveryAddressViewController.instantiate(fromAppStoryboard: .Home)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                let item = dataArray[indexPath.row]
                let vc = BeforeOrderPlaceListViewController.instantiate(fromAppStoryboard: .Address)
                vc.dataArray = self.cartArray
                vc.addressid = item["id"].stringValue
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
