//
//  SearchMapCell.swift
//  FoodAppUser
//
//  Created by Pyramidions on 17/10/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class SearchMapCell: UITableViewCell
{

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    
    @IBOutlet weak var seperatorLbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        locationLbl.text = ""
        detailLbl.text = ""
        setFont()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func setFont()
    {
        locationLbl.font = UIFont.appMediumFontWith(size: 14)
        detailLbl.font = UIFont.appRegularFontWith(size: 12)
    }
    
}
