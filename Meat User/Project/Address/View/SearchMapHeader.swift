//
//  SearchMapHeader.swift
//  FoodAppUser
//
//  Created by Pyramidions on 17/10/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class SearchMapHeader: UITableViewHeaderFooterView
{
    @IBOutlet weak var currentLocationBtn: UIButton!
    @IBOutlet weak var currentLocationLbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        setFont()
    }
    
    func setFont()
    {
//        currentLocationBtn.setTitle(CONSTANT.PINLOCATION, for: .normal)
        currentLocationLbl.text = CONSTANT.PINLOCATION
        currentLocationLbl.font = UIFont.appMediumFontWith(size: 14)
    }
}


