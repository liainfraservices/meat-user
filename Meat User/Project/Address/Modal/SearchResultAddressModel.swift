//
//  SearchResultAddressModel.swift
//  FoodAppUser
//
//  Created by Pyramidions on 05/12/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import Foundation


struct SearchResultAddressModel: Codable
{
    let place, placeID, complete: String?
}

struct AddressSearchModal : Codable
{
    let status : String?
    let results : [AddressSearchResults]?
    
    enum CodingKeys: String, CodingKey
    {
        case status = "status"
        case results = "results"
    }
    
    init(from decoder: Decoder) throws
    {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        results = try values.decodeIfPresent([AddressSearchResults].self, forKey: .results)
    }
    
}

struct Address_search_components : Codable {
    let types : [String]?
    let long_name : String?
    let short_name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case types = "types"
        case long_name = "long_name"
        case short_name = "short_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        types = try values.decodeIfPresent([String].self, forKey: .types)
        long_name = try values.decodeIfPresent(String.self, forKey: .long_name)
        short_name = try values.decodeIfPresent(String.self, forKey: .short_name)
    }
    
}


struct AddressSearchGeometry : Codable {
    let location_type : String?
    let viewport : AddressSearchViewport?
    let location : AddressSearchLocation?
    
    enum CodingKeys: String, CodingKey {
        
        case location_type = "location_type"
        case viewport = "viewport"
        case location = "location"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        location_type = try values.decodeIfPresent(String.self, forKey: .location_type)
        viewport = try values.decodeIfPresent(AddressSearchViewport.self, forKey: .viewport)
        location = try values.decodeIfPresent(AddressSearchLocation.self, forKey: .location)
    }
    
}

struct AddressSearchLocation : Codable {
    let lat : Double?
    let lng : Double?
    
    enum CodingKeys: String, CodingKey {
        
        case lat = "lat"
        case lng = "lng"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        lng = try values.decodeIfPresent(Double.self, forKey: .lng)
    }
    
}


struct AddressSearchNortheast : Codable {
    let lat : Double?
    let lng : Double?
    
    enum CodingKeys: String, CodingKey {
        
        case lat = "lat"
        case lng = "lng"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        lng = try values.decodeIfPresent(Double.self, forKey: .lng)
    }
    
}


struct AddressSearchPlus_code : Codable {
    let compound_code : String?
    let global_code : String?
    
    enum CodingKeys: String, CodingKey {
        
        case compound_code = "compound_code"
        case global_code = "global_code"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        compound_code = try values.decodeIfPresent(String.self, forKey: .compound_code)
        global_code = try values.decodeIfPresent(String.self, forKey: .global_code)
    }
    
}


struct AddressSearchResults : Codable {
    let plus_code : AddressSearchPlus_code?
    let geometry : AddressSearchGeometry?
    let formatted_address : String?
    let types : [String]?
    let place_id : String?
    let address_components : [Address_search_components]?
    
    enum CodingKeys: String, CodingKey {
        
        case plus_code = "plus_code"
        case geometry = "geometry"
        case formatted_address = "formatted_address"
        case types = "types"
        case place_id = "place_id"
        case address_components = "address_components"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        plus_code = try values.decodeIfPresent(AddressSearchPlus_code.self, forKey: .plus_code)
        geometry = try values.decodeIfPresent(AddressSearchGeometry.self, forKey: .geometry)
        formatted_address = try values.decodeIfPresent(String.self, forKey: .formatted_address)
        types = try values.decodeIfPresent([String].self, forKey: .types)
        place_id = try values.decodeIfPresent(String.self, forKey: .place_id)
        address_components = try values.decodeIfPresent([Address_search_components].self, forKey: .address_components)
    }
    
}


struct AddressSearchSouthwest : Codable {
    let lat : Double?
    let lng : Double?
    
    enum CodingKeys: String, CodingKey {
        
        case lat = "lat"
        case lng = "lng"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        lng = try values.decodeIfPresent(Double.self, forKey: .lng)
    }
    
}

struct AddressSearchViewport : Codable {
    let northeast : AddressSearchNortheast?
    let southwest : AddressSearchSouthwest?
    
    enum CodingKeys: String, CodingKey {
        
        case northeast = "northeast"
        case southwest = "southwest"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        northeast = try values.decodeIfPresent(AddressSearchNortheast.self, forKey: .northeast)
        southwest = try values.decodeIfPresent(AddressSearchSouthwest.self, forKey: .southwest)
    }
    
}


