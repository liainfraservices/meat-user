//
//  ReverseGeocodingModal.swift
//  GetTaxi
//
//  Created by Pyramidions on 17/09/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import Foundation

struct ReverseGeocodingModal: Codable {
    let plusCode: PlusCode?
    let results: [GeocodingResult]?
    let status: String?
    
    enum CodingKeys: String, CodingKey {
        case plusCode = "plus_code"
        case results = "results"
        case status = "status"
    }
}

// MARK: - PlusCode
struct PlusCode: Codable {
    let compoundCode: String?
    let globalCode: String?
    
    enum CodingKeys: String, CodingKey {
        case compoundCode = "compound_code"
        case globalCode = "global_code"
    }
}

// MARK: - Result
struct GeocodingResult: Codable {
    let addressComponents: [AddressComponent]?
    let formattedAddress: String?
    let geometry: GecodingGeometry?
    let placeid: String?
    let plusCode: PlusCode?
    let types: [String]?
    
    enum CodingKeys: String, CodingKey {
        case addressComponents = "address_components"
        case formattedAddress = "formatted_address"
        case geometry = "geometry"
        case placeid = "place_id"
        case plusCode = "plus_code"
        case types = "types"
    }
}

// MARK: - AddressComponent
struct AddressComponent: Codable {
    let longName: String?
    let shortName: String?
    let types: [String]?
    
    enum CodingKeys: String, CodingKey {
        case longName = "long_name"
        case shortName = "short_name"
        case types = "types"
    }
}

// MARK: - Geometry
struct GecodingGeometry: Codable {
    let location: GecodingLocation?
    let locationType: String?
    let viewport: GecodingBounds?
    let bounds: GecodingBounds?
    
    enum CodingKeys: String, CodingKey {
        case location = "location"
        case locationType = "location_type"
        case viewport = "viewport"
        case bounds = "bounds"
    }
}

// MARK: - Bounds
struct GecodingBounds: Codable {
    let northeast: GecodingLocation?
    let southwest: GecodingLocation?
    
    enum CodingKeys: String, CodingKey {
        case northeast = "northeast"
        case southwest = "southwest"
    }
}

// MARK: - Location
struct GecodingLocation: Codable {
    let lat: Double?
    let lng: Double?
    
    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case lng = "lng"
    }
}
