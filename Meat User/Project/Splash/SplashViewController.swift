//
//  ViewController.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Localize_Swift

class SplashViewController: UIViewController
{

    @IBOutlet weak var startBtn: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        startBtn.setTitle(CONSTANT.START, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        Localize.setCurrentLanguage("ar")

        setText()

        
//        if UserDefaults.standard.isLoggedIn()
//        {
        
        let vc = ListBranchViewController.instantiate(fromAppStoryboard: .Home)
        vc.isFromSplash = true
        self.navigationController?.pushViewController(vc, animated: true)

        
        /*
        
            let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
 */
//        }
    }

    @IBAction func startAtn(_ sender: Any)
    {
        let vc = SignInViewController.instantiate(fromAppStoryboard: .Splash)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}

