//
//  SignInViewController.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignInViewController: BaseViewController
{
    
    @IBOutlet weak var signInTitleLbl: UILabel!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var registerLaterBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var guestUserBtn: UIButton!
    
    let connection = Connection()
    
    var isFromBooking = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setComponents()
        
        /*
        if isFromBooking
        {
            guestUserBtn.isHidden = true
        }
        else
        {
            guestUserBtn.isHidden = false
        }*/
    }
    
    @IBAction func forgotPasswordAtn(_ sender: Any)
    {
        if emailTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_NAME)
        }
        else
        {
            forgotPassword()
        }
    }
    
    @IBAction func registerLaterAtn(_ sender: Any)
    {
        let vc = RegisterViewController.instantiate(fromAppStoryboard: .Splash)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signInAtn(_ sender: Any)
    {
        if emailTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_NAME)
        }
        else if passwordTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_PASSWORD)
        }
        else
        {
            checkLogin()
        }
    }
    
    @IBAction func guestUserAtn(_ sender: Any)
    {
        UserDefaults.standard.setUserId(value: "0")
        UserDefaults.standard.setLoggedIn(value: false)
        self.navigationController?.popViewController(animated: true)
        /*
        let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)*/
    }
    
    
}

extension SignInViewController
{
    func setComponents()
    {
        signInTitleLbl.font = UIFont.appMediumFontWith(size: 16)
        emailTxt.font = UIFont.appRegularFontWith(size: 14)
        passwordTxt.font = UIFont.appRegularFontWith(size: 14)
        forgotPasswordBtn.titleLabel?.font = UIFont.appRegularFontWith(size: 12)
        registerLaterBtn.titleLabel?.font = UIFont.appRegularFontWith(size: 12)
        signInBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        guestUserBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        
        signInTitleLbl.text = CONSTANT.SIGN_IN
        emailTxt.placeholder = CONSTANT.NAME_EMAIL
        passwordTxt.placeholder = CONSTANT.PASSWORD
        forgotPasswordBtn.setTitle(CONSTANT.FORGOT_PASSWORD, for: .normal)
        registerLaterBtn.setTitle(CONSTANT.REGISTER_LATER, for: .normal)
        signInBtn.setTitle(CONSTANT.SIGN_IN, for: .normal)
        guestUserBtn.setTitle(CONSTANT.GUEST_USER, for: .normal)
    }
    
    func forgotPassword()
    {
        let url =  APIList().getUrlString(url: .FORGOTPASSWORD)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]

        let parameter : Parameters = [
            "email":emailTxt.text!,
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func checkLogin()
    {
        let url =  APIList().getUrlString(url: .LOGIN)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]

        let parameter : Parameters = [
            "password": passwordTxt.text!,
            "email":emailTxt.text!,
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                UserDefaults.standard.setLoggedIn(value: true)
                UserDefaults.standard.setUserId(value: data["data"]["userID"].stringValue)

//                UserDefaults.standard.setAccessToken(value: data["data"]["token"].stringValue)
//                UserDefaults.standard.setEmailId(value: data["data"]["email"].stringValue)
//                UserDefaults.standard.setMobileNo(value: data["userDeatils"]["mobile"].stringValue)
//                UserDefaults.standard.setCountryCode(value: data["data"]["countryCode"].stringValue)
//                UserDefaults.standard.setLatitude(value: data["data"]["latitude"].stringValue)
//                UserDefaults.standard.setLongitude(value: data["data"]["longitude"].stringValue)
//                UserDefaults.standard.setUserId(value: data["data"]["id"].stringValue)
//                UserDefaults.standard.setName(value: data["data"]["name"].stringValue)
//                UserDefaults.standard.setProfilePic(value: data["data"]["profilePic"].stringValue)
//                UpdateDeviceToken()
                self.moveHome()
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func moveHome()
    {
        if isFromBooking
        {
            for controller in self.navigationController!.viewControllers as Array
            {
                if controller.isKind(of: CategoryViewController.self)
                {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
            
            /*
            let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)*/
        }
    }
}
