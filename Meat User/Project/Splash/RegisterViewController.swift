//
//  RegisterViewController.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterViewController: BaseViewController {

    
    @IBOutlet weak var registerTitleLbl: UILabel!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var mobileNoTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var alreadyAccountBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
    let connection = Connection()
    var isFromBooking = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        setComponents()
        // Do any additional setup after loading the view.
    }
    @IBAction func alreadyHaveAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpAtn(_ sender: Any)
    {
        if nameTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_NAME_REGISTER)
        }
        else if emailTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_EMAIL)
        }
        else if mobileNoTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_MOBILE)
        }
        else if passwordTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_PASSWORD)
        }
        else if confirmPasswordTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_CONFIRM_PASSWORD)
        }
        else if passwordTxt.text != confirmPasswordTxt.text
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PASSWORD_SAME)
        }
        else
        {
            createAccount()
        }
    }
    
    
}

extension RegisterViewController
{
    func setComponents()
    {
        registerTitleLbl.font = UIFont.appMediumFontWith(size: 16)
        nameTxt.font = UIFont.appRegularFontWith(size: 14)
        emailTxt.font = UIFont.appRegularFontWith(size: 14)
        mobileNoTxt.font = UIFont.appRegularFontWith(size: 14)
        passwordTxt.font = UIFont.appRegularFontWith(size: 14)
        confirmPasswordTxt.font = UIFont.appRegularFontWith(size: 14)
        alreadyAccountBtn.titleLabel?.font = UIFont.appRegularFontWith(size: 12)
        signUpBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        
        registerTitleLbl.text = CONSTANT.REGISTER
        nameTxt.placeholder = CONSTANT.NAME
        emailTxt.placeholder = CONSTANT.EMAIL
        mobileNoTxt.placeholder = CONSTANT.MOBILE_NO
        passwordTxt.placeholder = CONSTANT.PASSWORD
        confirmPasswordTxt.placeholder = CONSTANT.CONFIRM_PASSWORD
        alreadyAccountBtn.setTitle(CONSTANT.ALREADY_ACCOUNT, for: .normal)
        signUpBtn.setTitle(CONSTANT.SIGN_UP, for: .normal)

    }
    
    func createAccount()
    {
        let url =  APIList().getUrlString(url: .CREATE_ACCOUNT)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]

        let parameter : Parameters = [
            "name":emailTxt.text!,
            "mobile":mobileNoTxt.text!,
            "password": passwordTxt.text!,
            "email":emailTxt.text!,
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                UserDefaults.standard.setLoggedIn(value: true)
                UserDefaults.standard.setUserId(value: data["data"]["custId"].stringValue)

//                UserDefaults.standard.setAccessToken(value: data["data"]["token"].stringValue)
//                UserDefaults.standard.setEmailId(value: data["data"]["email"].stringValue)
//                UserDefaults.standard.setMobileNo(value: data["userDeatils"]["mobile"].stringValue)
//                UserDefaults.standard.setCountryCode(value: data["data"]["countryCode"].stringValue)
//                UserDefaults.standard.setLatitude(value: data["data"]["latitude"].stringValue)
//                UserDefaults.standard.setLongitude(value: data["data"]["longitude"].stringValue)
//                UserDefaults.standard.setUserId(value: data["data"]["id"].stringValue)
//                UserDefaults.standard.setName(value: data["data"]["name"].stringValue)
//                UserDefaults.standard.setProfilePic(value: data["data"]["profilePic"].stringValue)
//                UpdateDeviceToken()
                self.moveHome()
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func moveHome()
    {
        if isFromBooking
        {
            for controller in self.navigationController!.viewControllers as Array
            {
                if controller.isKind(of: CategoryViewController.self)
                {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
            let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }

}
