//
//  BeforeOrderTableViewCell.swift
//  Meat User
//
//  Created by Jai on 11/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

class BeforeOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var goatNameLbl: UILabel!
    @IBOutlet weak var productDesLbl: UILabel!
    @IBOutlet weak var khemaLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    
    @IBOutlet weak var qtyTitleLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTextComponents()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class var identifier: String
    {
        return String(describing: self)
    }
    
    class var nib: UINib
    {
        return UINib(nibName: identifier, bundle: nil)
    }

    
}

extension BeforeOrderTableViewCell
{
    func setTextComponents()
    {
        goatNameLbl.font = UIFont.appMediumFontWith(size: 14)
        khemaLbl.font = UIFont.appRegularFontWith(size: 12)
        totalLbl.font = UIFont.appRegularFontWith(size: 12)
        quantityLbl.font = UIFont.appRegularFontWith(size: 12)
        productDesLbl.font = UIFont.appRegularFontWith(size: 12)
        qtyTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        
        qtyTitleLbl.text = CONSTANT.QTY
        quantityLbl.text = ""
        goatNameLbl.text = ""
        khemaLbl.text = ""
        totalLbl.text = ""
        productDesLbl.text = ""

    }
}
