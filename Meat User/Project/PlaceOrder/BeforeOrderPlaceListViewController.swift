//
//  OrderListViewController.swift
//  Meat User
//
//  Created by Jai on 11/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BeforeOrderPlaceListViewController: BaseViewController
{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var subTotalTitleLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!

    @IBOutlet weak var vatTitleLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!

    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!

    @IBOutlet weak var continueBtn: UIButton!
    
    var dataArray: [JSON] = []
    
    var totalPrice = 0
    let connection = Connection()
    var addressid = ""
    
    
    @IBOutlet weak var codImageView: UIImageView!
    @IBOutlet weak var codLbl: UILabel!

    @IBOutlet weak var cocImageView: UIImageView!
    @IBOutlet weak var cocLbl: UILabel!
    
    var isCod = true
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        tableView?.register(BeforeOrderTableViewCell.nib, forCellReuseIdentifier: BeforeOrderTableViewCell.identifier)
//        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = false
        tableView.contentInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 10, right: 0)
        
        
        setTextComponents()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        
        if let obj = object as? UITableView
        {
            if obj == self.tableView && keyPath == "contentSize"
            {
                if let newvalue = change?[.newKey]
                {
                    let newsize  = newvalue as! CGSize
                    self.tableViewHeight.constant = newsize.height
                }
            }
        }
    }

    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueAtn(_ sender: Any)
    {
        placeOrder()
    }
    
    @IBAction func codAtn(_ sender: Any)
    {
        isCod = true
        codImageView.image = UIImage.init(named: "male_selected")
        cocImageView.image = UIImage.init(named: "male_unselected")
    }
    
    @IBAction func cocAtn(_ sender: Any)
    {
        isCod = false
        cocImageView.image = UIImage.init(named: "male_selected")
        codImageView.image = UIImage.init(named: "male_unselected")
    }
    
}

extension BeforeOrderPlaceListViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = dataArray[indexPath.row]["details"][0]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BeforeOrderTableViewCell.identifier, for: indexPath) as! BeforeOrderTableViewCell

        let quantity = item["qty"].intValue
        let khemmaPrice = item["kheemaDetails"]["price"].intValue
        let price = item["productDetails"]["price"].intValue
        let totalPrice = (price * quantity) + khemmaPrice
 
        cell.quantityLbl.text = "\(quantity)"
        cell.goatNameLbl.text = item["goatName"].stringValue
                        
        
        let attibute = [ NSAttributedString.Key.font : UIFont.appMediumFontWith(size: 12) ]
        let attibuteRegular = [ NSAttributedString.Key.font : UIFont.appRegularFontWith(size: 12) ]

        
        var productDes = NSMutableAttributedString()
        productDes.append(NSAttributedString(string: (item["productDetails"]["desc"].stringValue), attributes: attibute))
        productDes.append(NSAttributedString(string: " "))
        productDes.append(NSAttributedString(string: (CONSTANT.WITH), attributes: attibuteRegular))
        productDes.append(NSAttributedString(string: " "))
        productDes.append(NSAttributedString(string: (item["cuttingStyle"]["desc"].stringValue), attributes: attibute))
        productDes.append(NSAttributedString(string: " "))
        productDes.append(NSAttributedString(string: (CONSTANT.CUTTING), attributes: attibuteRegular))
        productDes.append(NSAttributedString(string: " "))
        productDes.append(NSAttributedString(string: (CONSTANT.IN), attributes: attibuteRegular))
        productDes.append(NSAttributedString(string: "\n"))
        productDes.append(NSAttributedString(string: (item["packingStyle"]["desc"].stringValue), attributes: attibute))
        productDes.append(NSAttributedString(string: " "))
        productDes.append(NSAttributedString(string: (CONSTANT.PACKING), attributes: attibuteRegular))

        cell.productDesLbl.attributedText = productDes
        
        cell.productDesLbl.lineBreakMode = .byWordWrapping;
        cell.productDesLbl.numberOfLines = 0;
        cell.productDesLbl.sizeToFit()

        
//        cell.productDesLbl.text = "\(item["productDetails"]["pDesc"].stringValue) \(CONSTANT.WITH) \(item["cuttingStyle"]["cName"].stringValue) \(CONSTANT.CHIPPING) \(CONSTANT.IN) \(item["packingStyle"]["pName"].stringValue) \(CONSTANT.PACKING_STYLE)"
        
        cell.khemaLbl.text = "\(CONSTANT.KHEEMA) - \(item["kheemaDetails"]["desc"].stringValue)"
        
        cell.totalLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*
        let item = dataArray[indexPath.row]["details"][0]
        let vc = UpdateCategoryViewController.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.productID = item["productId"].stringValue
        vc.goatName = item["goatName"].stringValue
        vc.goatImage = item["goatImage"].stringValue
        vc.dataArray = item
        vc.cartid = dataArray[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}

extension BeforeOrderPlaceListViewController
{
    func setTextComponents()
    {
        subTotalTitleLbl.font = UIFont.appMediumFontWith(size: 12)
        subTotalLbl.font = UIFont.appMediumFontWith(size: 12)

        vatTitleLbl.font = UIFont.appMediumFontWith(size: 12)
        vatLbl.font = UIFont.appMediumFontWith(size: 12)

        totalTitleLbl.font = UIFont.appMediumFontWith(size: 12)
        totalLbl.font = UIFont.appMediumFontWith(size: 12)
                
        codLbl.font = UIFont.appMediumFontWith(size: 12)
        cocLbl.font = UIFont.appMediumFontWith(size: 12)
        
        
        subTotalTitleLbl.text = CONSTANT.SUBTOTAL
        vatTitleLbl.text = CONSTANT.VAT
        totalTitleLbl.text = CONSTANT.TOTAL

        codLbl.text = CONSTANT.COD
        cocLbl.text = CONSTANT.COC
        
        continueBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        continueBtn.setTitle(CONSTANT.FOLLOWUP, for: .normal)

        for (index, element) in self.dataArray.enumerated()
        {
            let item = element["details"][0]
            let quantity = item["qty"].intValue
            let khemmaPrice = item["kheemaDetails"]["price"].intValue
            let price = item["productDetails"]["price"].intValue
            let totalPrice = (price * quantity) + khemmaPrice
            self.totalPrice = self.totalPrice + totalPrice
        }
        
        let value = "\(self.totalPrice) \(CURRENCY_TYPE)"
        self.subTotalLbl.text = value
        self.vatLbl.text = "\(ConstantManager.appConstant.VAT) %"
        
        if let vat = Double(ConstantManager.appConstant.VAT)
        {
            let percentageVAT = vat / 100.0
            self.totalLbl.text = "\((Double(self.totalPrice) * percentageVAT) + Double(self.totalPrice)) \(CURRENCY_TYPE)"
        }
        else
        {
            self.vatLbl.text = "0 %"
            self.totalLbl.text = "\(Double(self.totalPrice)) \(CURRENCY_TYPE)"
        }
        
    }
    
    func placeOrder()
    {
        let url =  APIList().getUrlString(url: .PLACE_ORDER)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        var paymentType = ""
        if isCod
        {
            paymentType = "cash"
        }
        else
        {
            paymentType = "card"
        }
        
        let parameter : Parameters = [
            "addressid": addressid,
            "userid": UserDefaults.standard.getUserId(),
            "paymentType":paymentType
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
//                {
//                    data =     {
//                        orderId = 50;
//                    };
//                    error = 0;
//                    message = "order placed successfully";
//                }
                self.orderConfirmed(orderID: data["data"]["orderId"].stringValue)
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func orderConfirmed(orderID: String)
    {
        let vc = ThanksViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .custom
        vc.appNo = orderID
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)

//        ThanksViewController
        
        /*
        let alert = UIAlertController(title: CONSTANT.SUCCESS, message: CONSTANT.CONFIRMED_PROCEESED,         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: CONSTANT.OK,
                                      style: UIAlertAction.Style.default,
                                      handler:
        {
            (_: UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)*/
    }

}

extension BeforeOrderPlaceListViewController: ThanksConsultDelegate
{
    func didThanksConsult()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
