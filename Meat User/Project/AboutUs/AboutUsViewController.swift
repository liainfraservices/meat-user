//
//  AboutUsViewController.swift
//  Meat User
//
//  Created by Jai on 09/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController
{

    var isHelp = false
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var contentLbl: UILabel!
    var content = ""
    var contactNo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentLbl.font = UIFont.appMediumFontWith(size: 14)
        contentLbl.text = ""
        
        if isHelp
        {
            callView.isHidden = false
        }
        else
        {
            callView.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        contentLbl.text = content
    }
    
    @IBAction func makeCallAtn(_ sender: Any)
    {
        let contact = "\(CONSTANT.YOU_CAN_CONTACT) \(contactNo)"
        self.showAlertError(titleStr: CONSTANT.CONTACT_US, messageStr: contact)
    }
    
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
