//
//  CategoryViewController.swift
//  Meat User
//
//  Created by Jai on 30/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UpdateCategoryViewController: BaseViewController
{

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sizeTitleLbl: UILabel!
    @IBOutlet weak var sizeKGLbl: UILabel!
    @IBOutlet weak var chippingTitleLbl: UILabel!
    @IBOutlet weak var mediumLbl: UILabel!
    @IBOutlet weak var quantityTitleLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var kheemaTitleLbl: UILabel!
    @IBOutlet weak var kheemaLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var packingStyleLbl: UILabel!
    @IBOutlet weak var notesTxtView: UITextView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var addToCartBtn: UIButton!
    var placeholderLabel : UILabel!
    let connection = Connection()
    var productID = ""
    
    var sizeID = ""
    var cuttingID = ""
    var packingID = ""
    var khemaID = ""
    var quantity = 1
    
    var khemaPrice = 0
    var productPrice = 0
    
    var dataArray = JSON()
    var params = JSON()
    var goatName = ""
    var goatImage = ""
    var notes = ""
    var cartid = ""
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        mainView.isHidden = true
        notesTxtView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = CONSTANT.NOTES
        placeholderLabel.font = UIFont.appMediumFontWith(size: 14)
        placeholderLabel.sizeToFit()
        notesTxtView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (notesTxtView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !notesTxtView.text.isEmpty
        
        priceView.addDashedBorder()
        setTextComponents()
        
//        getProducts()
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sizeKGAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = dataArray["ProductDetails"].arrayValue
        vc.vehicleTitle = CONSTANT.SIZE
        vc.type = 1
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
            
            self.sizeKGLbl.text = data["pDesc"].stringValue
            let price = data["pPrice"].intValue
            let totalPrice = (price * self.quantity) + self.khemaPrice
            self.priceLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
            self.sizeID = data["ProductDetailsId"].stringValue
            self.productPrice = data["pPrice"].intValue
            self.params["productDetails"].dictionaryObject = data.dictionaryObject!
        }

    }
    
    @IBAction func mediumAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = dataArray["cuttingStyle"].arrayValue
        vc.vehicleTitle = CONSTANT.CHIPPING
        vc.type = 2
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
            
            self.mediumLbl.text = data["cName"].stringValue
            self.cuttingID = data["cId"].stringValue
            self.params["packingStyle"].dictionaryObject = data.dictionaryObject!
        }

    }
    
    @IBAction func quantityIncAtn(_ sender: Any)
    {
        if quantity > 1
        {
            quantity = quantity - 1
            quantityLbl.text = "\(quantity)"
            let totalPrice = (productPrice * quantity) + khemaPrice
            priceLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
            params["qty"].stringValue = "\(quantity)"
        }
    }
    
    @IBAction func quantityDecAtn(_ sender: Any)
    {
        quantity = quantity + 1
        quantityLbl.text = "\(quantity)"
        let totalPrice = (productPrice * quantity) + khemaPrice
        priceLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
        params["qty"].stringValue = "\(quantity)"
    }
    
    @IBAction func kheemaAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = dataArray["kheemaDetails"].arrayValue
        vc.vehicleTitle = CONSTANT.KHEEMA
        vc.type = 3
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
                        
            self.kheemaLbl.text = data["kDesc"].stringValue
            let price = (self.productPrice * self.quantity) + data["kPrice"].intValue
            self.priceLbl.text = "\(price) \(CURRENCY_TYPE)"
            self.khemaID = data["kId"].stringValue
            self.khemaPrice = data["kPrice"].intValue
            self.params["kheemaDetails"].dictionaryObject = data.dictionaryObject!
        }

    }
    
    @IBAction func packingStyleAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = dataArray["packingStyle"].arrayValue
        vc.vehicleTitle = CONSTANT.PACKING_STYLE
        vc.type = 4
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
            
            self.packingStyleLbl.text = data["pName"].stringValue
            self.packingID = data["pId"].stringValue
            self.params["packingStyle"].dictionaryObject = data.dictionaryObject!
        }

    }
    
    @IBAction func continueAtn(_ sender: Any)
    {
//        if notesTxtView.text!.isEmpty
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PLEASE_ENTER_NOTES)
//        }
//        else
//        {
            self.params["notes"].stringValue = notesTxtView.text!
            self.addCart(isDelivery: false)
//        }
    }
    /*{
        let vc = DeliveryAddressViewController.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }*/
    
    @IBAction func addToCartAtn(_ sender: Any)
    {
//        if notesTxtView.text!.isEmpty
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PLEASE_ENTER_NOTES)
//        }
//        else
//        {
            self.params["notes"].stringValue = notesTxtView.text!
            self.addCart(isDelivery: false)
//        }
    }
    
}

extension UpdateCategoryViewController
{
    func setTextComponents()
    {
        sizeTitleLbl.text = CONSTANT.SIZE
        chippingTitleLbl.text = CONSTANT.CHIPPING
        quantityTitleLbl.text = CONSTANT.QUANTITY
        kheemaTitleLbl.text = CONSTANT.KHEEMA
        packingStyleLbl.text = CONSTANT.PACKING_STYLE
        notesTxtView.text = ""
        continueBtn.setTitle(CONSTANT.UPDATE_TO_CART, for: .normal)
        addToCartBtn.setTitle(CONSTANT.UPDATE_TO_CART, for: .normal)
        
        sizeTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        sizeKGLbl.font = UIFont.appMediumFontWith(size: 14)
        chippingTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        mediumLbl.font = UIFont.appMediumFontWith(size: 14)
        quantityTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        quantityLbl.font = UIFont.appMediumFontWith(size: 14)
        kheemaTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        kheemaLbl.font = UIFont.appMediumFontWith(size: 14)
        priceLbl.font = UIFont.appMediumFontWith(size: 14)
        packingStyleLbl.font = UIFont.appMediumFontWith(size: 14)
        notesTxtView.font = UIFont.appMediumFontWith(size: 14)
        continueBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        addToCartBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        
        sizeKGLbl.text = ""
        mediumLbl.text = ""
        quantityLbl.text = ""
        kheemaLbl.text = ""
        priceLbl.text = ""
        packingStyleLbl.text = ""
        
        setValue(data: self.dataArray)
    }
    
    func addCart(isDelivery: Bool)
    {
        let url =  APIList().getUrlString(url: .UPDATE_CART)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
//        let parameter = String(describing: params)
        
        
        let parameter: Parameters = [
            "cartid": cartid,
            "userid": UserDefaults.standard.getUserId(),
            "qty": quantityLbl.text!
        ]
        

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                if isDelivery
                {
                    
                }
                else
                {
                    self.goBack(content: data["message"].stringValue)
                }
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func goBack(content: String)
    {
        let alert = UIAlertController(title: CONSTANT.OOPS, message: content,         preferredStyle: UIAlertController.Style.alert)
        
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: CONSTANT.OOPS as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(myMutableString, forKey: "attributedTitle")
        
        var messageMutableString = NSMutableAttributedString()
        messageMutableString = NSMutableAttributedString(string: content as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(messageMutableString, forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: CONSTANT.OK,
                                      style: UIAlertAction.Style.default,
                                      handler:
            {
                (_: UIAlertAction!) in
                self.getBack()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getBack()
    {
        self.navigationController?.popViewController(animated: true)
    }

    func getProducts()
    {
        let url =  APIList().getUrlString(url: .PRODUCT_DETAILS)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "product": self.productID,
            "branch": ConstantManager.appConstant.branchID ?? 0
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                if (data["data"].dictionary != nil)
                {
                    self.setValue(data: data["data"])
                }
                else
                {
                    self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
                }
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func setValue(data: JSON)
    {
        dataArray = data
        mainView.isHidden = false
        
        params["goatImage"].stringValue = self.goatImage
        params["goatName"].stringValue = self.goatName

        params["userId"].stringValue = UserDefaults.standard.getUserId()
        params["productId"].stringValue = data["productId"].stringValue
        params["qty"].stringValue = data["qty"].stringValue
        params["baranchId"].stringValue = ConstantManager.appConstant.branchID ?? "0"
        
        var price = 0
        
        quantity = data["qty"].intValue
        quantityLbl.text = "\(quantity)"
        
        if let productDetails = data["productDetails"].dictionaryObject
        {
                sizeKGLbl.text = data["productDetails"]["pDesc"].stringValue
                price = data["productDetails"]["pPrice"].intValue
                priceLbl.text = "\(data["productDetails"]["pPrice"].stringValue) \(CURRENCY_TYPE)"
                sizeID = data["productDetails"]["ProductDetailsId"].stringValue
                productPrice = data["productDetails"]["pPrice"].intValue
                params["productDetails"].dictionaryObject = productDetails
        }
        
        if let productDetails = data["cuttingStyle"].dictionaryObject
        {
                mediumLbl.text = data["cuttingStyle"]["cName"].stringValue
                cuttingID = data["cuttingStyle"]["cId"].stringValue
                params["cuttingStyle"].dictionaryObject = productDetails
        }
        
        if let productDetails = data["packingStyle"].dictionaryObject
        {
                packingStyleLbl.text = data["packingStyle"]["pName"].stringValue
                packingID = data["packingStyle"]["pId"].stringValue
                params["packingStyle"].dictionaryObject = productDetails
        }

        if let productDetails = data["kheemaDetails"].dictionaryObject
        {
                kheemaLbl.text = data["kheemaDetails"]["kDesc"].stringValue
                let price = price + data["kheemaDetails"]["kPrice"].intValue
                priceLbl.text = "\(price) \(CURRENCY_TYPE)"
                khemaID = data["kheemaDetails"]["kId"].stringValue
                khemaPrice = data["kheemaDetails"]["kPrice"].intValue
                params["kheemaDetails"].dictionaryObject = productDetails
        }
        
        productPrice = (data["productDetails"]["pPrice"].intValue) * quantity
        priceLbl.text = "\(productPrice) \(CURRENCY_TYPE)"
        print("Params = ",String(describing: params))
        
    }
}

extension UpdateCategoryViewController: UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}

