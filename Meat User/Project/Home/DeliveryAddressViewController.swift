//
//  DeliveryAddressViewController.swift
//  Meat User
//
//  Created by Jai on 04/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class DeliveryAddressViewController: BaseViewController
{
    
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var streetTxt: UITextField!
    @IBOutlet weak var nameOfBuildingTxt: UITextField!
    @IBOutlet weak var floorTxt: UITextField!
    @IBOutlet weak var flatTxt: UITextField!
    
    @IBOutlet weak var continueToPayBtn: UIButton!
    
    let connection = Connection()
    var params = JSON()
    
    var completelatitude = ""
    var completelongtitude = ""
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setComponents()
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getAddressFromMapAtn(_ sender: Any)
    {
        let vc = AddressSearchViewController.instantiate(fromAppStoryboard: .Address)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func continueToPayAtn(_ sender: Any)
    {
        
        if cityTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PLEASE_CHOOSE_ADDRESS)
        }
        else if streetTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_PHONE_NO)
        }
        else if nameOfBuildingTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_NAME_OF_BUILDING)
        }
        else if flatTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PLEASE_ENTER_FLAT_NO)
        }
        else if completelatitude.isEmpty && completelongtitude.isEmpty
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.YOU_NEED_PICK_LOCATION)
        }
        else
        {
            self.addAddress()
        }
        
        /*
        let vc = ThanksViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)*/
    }
    
}

extension DeliveryAddressViewController
{
    func setComponents()
    {
        cityTxt.font = UIFont.appRegularFontWith(size: 14)
        streetTxt.font = UIFont.appRegularFontWith(size: 14)
        nameOfBuildingTxt.font = UIFont.appRegularFontWith(size: 14)
        floorTxt.font = UIFont.appRegularFontWith(size: 14)
        flatTxt.font = UIFont.appRegularFontWith(size: 14)
        continueToPayBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        streetTxt.keyboardType = .phonePad
        
        cityTxt.placeholder = CONSTANT.ADDRESS
        streetTxt.placeholder = CONSTANT.PHONE_NUMBER
        nameOfBuildingTxt.placeholder = CONSTANT.NAME_OF_BUILDING
        floorTxt.placeholder = CONSTANT.FLOOR
        flatTxt.placeholder = CONSTANT.FLAT
        continueToPayBtn.setTitle(CONSTANT.SAVED_ADDRESS, for: .normal)
        
    }
}

extension DeliveryAddressViewController: AddressSearchViewControllerDelegate
{
    func didSelectLocation(place: String, address: String, latitude: String, longitude: String, location: AddressSearchLocation?)
    {
        ConstantManager.appConstant.latitude = latitude
        ConstantManager.appConstant.longtitude = longitude
        
        ConstantManager.appConstant.completeAddress = address
        
        completelatitude = latitude
        completelongtitude = longitude

        
        self.cityTxt.text = address
        
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.roundingMode = NumberFormatter.RoundingMode.floor
        formatter.maximumFractionDigits = 6

        let location: CLLocation = CLLocation(latitude: location?.lat ?? 0.0,
        longitude: location?.lng ?? 0.0)
//        self.preFillLocation(coords: location)
                
    }
    
    func preFillLocation(coords: CLLocation)
    {
        CLGeocoder().reverseGeocodeLocation(coords)
        {
            (placemark, error) in
            if error != nil
            {
                print("Hay un error")
            }
            else
            {
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    
                    print("Place = ",place)
                    
                    if place.thoroughfare != nil
                    {
                        self.nameOfBuildingTxt.text = place.thoroughfare ?? ""
                    }
                    if place.subThoroughfare != nil
                    {
                        self.cityTxt.text = place.locality ?? ""
                    }
                    if place.locality != nil
                    {
//                        self.streetTxt.text = place.subThoroughfare ?? ""
                    }
                }
            }
        }
    }
}

extension DeliveryAddressViewController: ThanksConsultDelegate
{
    func didThanksConsult()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension DeliveryAddressViewController
{
    func addAddress()
    {
        
        self.params["userId"].stringValue = UserDefaults.standard.getUserId()
        self.params["latitude"].stringValue = completelatitude
        self.params["longitude"].stringValue = completelongtitude
        self.params["address"].stringValue = cityTxt.text!
        self.params["buildingname"].stringValue = self.nameOfBuildingTxt.text!
        self.params["flatno"].stringValue = self.flatTxt.text!
        self.params["phone"].stringValue = self.streetTxt.text!
        
        let url =  APIList().getUrlString(url: .ADD_ADDRESS)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        showLottie()

        connection.requestPOSTRawData(url, params: String(describing: self.params), headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.back()
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func back()
    {
        self.navigationController?.popViewController(animated: true)
    }
}
