//
//  HomeViewController.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SwiftyJSON

class HomeViewController: BaseViewController
{
    
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var cartView: CustomView!
    @IBOutlet weak var cartLbl: UILabel!
    
    @IBOutlet weak var selectBranchTitleLbl: UILabel!
    @IBOutlet weak var selectBranchLbl: UILabel!

    let connection = Connection()    
    var dataArray = JSON()
    
    var aboutus = ""
    var help = ""
    var contactNo = ""
    
    var timer = Timer()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        cartView.isHidden = true
        cartLbl.font = UIFont.appRegularFontWith(size: 8)
        selectBranchTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        selectBranchLbl.font = UIFont.appMediumFontWith(size: 14)
        selectBranchTitleLbl.text = CONSTANT.SELECT_BRANCH_TITLE
        
        let bannerLayout = UICollectionViewFlowLayout()
        bannerLayout.scrollDirection = .horizontal
        bannerCollectionView?.setCollectionViewLayout(bannerLayout, animated: true)

        let categoryLayout = UICollectionViewFlowLayout()
        categoryLayout.scrollDirection = .vertical
        categoryCollectionView?.setCollectionViewLayout(categoryLayout, animated: true)

        bannerCollectionView.register(BannerCollectionViewCell.nib, forCellWithReuseIdentifier: BannerCollectionViewCell.identifier)

        categoryCollectionView.register(CategoryCollectionViewCell.nib, forCellWithReuseIdentifier: CategoryCollectionViewCell.identifier)
        
        setDelegate(view: self.bannerCollectionView)
        setDelegate(view: self.categoryCollectionView)
                
    }
    
    func setDelegate(view: UICollectionView)
    {
        view.delegate = self
        view.dataSource = self
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        if self.timer.isValid
        {
            self.timer.invalidate()
        }
        
//        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        getHome()
        
        if let branch = UserDefaults.standard.getBranch()
        {
            selectBranchLbl.text = branch["address"].stringValue
            ConstantManager.appConstant.branchID = branch["address_id"].stringValue
            ConstantManager.appConstant.branchName = branch["address"].stringValue
        }
        
        if self.dataArray["banner"].arrayValue.count > 0
        {
            if !self.timer.isValid
            {
                self.startTimer()
            }
        }

        
    }
    
    func startTimer() {

        self.timer =  Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    var x = 1

    @objc func scrollAutomatically(_ timer1: Timer)
    {
        if self.x < self.dataArray["banner"].arrayValue.count
        {
          let indexPath = IndexPath(item: x, section: 0)
          self.bannerCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
          self.x = self.x + 1
        }
        else
        {
          self.x = 0
          self.bannerCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }

    
    @IBAction func selectBranchAtn(_ sender: Any)
    {
        let vc = ListBranchViewController.instantiate(fromAppStoryboard: .Home)
        vc.dataArray = self.dataArray["branches"].arrayValue
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func cartAtn(_ sender: Any)
    {
        if UserDefaults.standard.isLoggedIn()
        {
            self.tabBarController?.selectedIndex = 1
        }
        else
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.LOGIN_ACCESS_FEATURES)
        }
    }
    

}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == bannerCollectionView
        {
            return self.dataArray["banner"].arrayValue.count
        }
        else
        {
            return self.dataArray["category"].arrayValue.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == bannerCollectionView
        {
            let item = self.dataArray["banner"][indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCollectionViewCell.identifier, for: indexPath) as! BannerCollectionViewCell
            let image = "\(IMAGE_BASE_URL)\(item["image"].stringValue)"
            ImageLoader().imageLoad(imgView: cell.imgView, url: item["image"].stringValue)
            return cell
        }
        else
        {
            let item = self.dataArray["category"][indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.identifier, for: indexPath) as! CategoryCollectionViewCell
            let image = "\(IMAGE_BASE_URL)\(item["image"].stringValue)"
            ImageLoader().imageLoad(imgView: cell.imgView, url: item["image"].stringValue)
            cell.nameLbl.text = item["pName"].stringValue
            return cell
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView != bannerCollectionView
        {
            if ConstantManager.appConstant.branchID != nil
            {
                let vc = CategoryViewController.instantiate(fromAppStoryboard: .Home)
                vc.hidesBottomBarWhenPushed = true
                vc.productID = self.dataArray["category"][indexPath.row]["pId"].stringValue
                vc.goatName = self.dataArray["category"][indexPath.row]["pName"].stringValue
                vc.goatImage = self.dataArray["category"][indexPath.row]["image"].stringValue
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.SELECT_BRANCH)
            }
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == bannerCollectionView
        {
            return CGSize(width: screenWidth, height: 170)
        }
        else
        {
            return CGSize(width: screenWidth/3 - 10, height: screenWidth/3 - 10)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == bannerCollectionView
        {
            return 0
        }
        else
        {
            return 10
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == bannerCollectionView
        {
            return 0
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        if collectionView == bannerCollectionView
        {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        else
        {
            return UIEdgeInsets(top: 20, left: 10, bottom: 0, right: 10)
        }
    }
}

extension HomeViewController
{
     override func prepare(for segue: UIStoryboardSegue, sender: Any?)
     {
         guard let sideMenuNavigationController = segue.destination as? SideMenuNavigationController else
         { return }
        
        sideMenuNavigationController.leftSide = false
//            if let lang = UserDefaults.standard.getLanguage()
//            {
//                if lang == Language.arabic.rawValue
//                {
//                    sideMenuNavigationController.leftSide = false
//                }
//                else
//                {
//                    sideMenuNavigationController.leftSide = true
//                }
//            }
//            else
//            {
//                sideMenuNavigationController.leftSide = true
//            }
         sideMenuNavigationController.settings = makeSettings()
     }
     
     
     private func selectedPresentationStyle() -> SideMenuPresentationStyle
     {
            let modes: [SideMenuPresentationStyle] = [.menuSlideIn, .viewSlideOut, .viewSlideOutMenuIn, .menuDissolveIn]
            return modes[0]
     }

        
    private func makeSettings() -> SideMenuSettings
    {
           let presentationStyle = selectedPresentationStyle()
     //              presentationStyle.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
            presentationStyle.menuStartAlpha = 1.0
         presentationStyle.menuScaleFactor = 1
     //            presentationStyle.onTopShadowOpacity = 1.0
     //           presentationStyle.presentingEndAlpha = 1.0
        presentationStyle.presentingScaleFactor = 1.0

           var settings = SideMenuSettings()
           settings.presentationStyle = presentationStyle
           settings.menuWidth = 300
           let styles:[UIBlurEffect.Style?] = [nil, .dark, .light, .extraLight]
           settings.blurEffectStyle = styles[0]
           settings.statusBarEndAlpha = 0

           return settings
     }

}


extension HomeViewController: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool)
    {
        print("SideMenu Disappearing! (animated: \(animated))")
        if ConstantManager.appConstant.sideMenuSelectIndex == 1
        {
            resetMenu()
        }
        else if ConstantManager.appConstant.sideMenuSelectIndex == 2
        {
            resetMenu()
            
            let vc = AboutUsViewController.instantiate(fromAppStoryboard: .Home)
            vc.content = self.aboutus
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else if ConstantManager.appConstant.sideMenuSelectIndex == 3
        {
            resetMenu()
            
            let vc = AboutUsViewController.instantiate(fromAppStoryboard: .Home)
            vc.hidesBottomBarWhenPushed = true
            vc.content = self.help
            vc.isHelp = true
            vc.contactNo = self.contactNo
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else if ConstantManager.appConstant.sideMenuSelectIndex == 4
        {
            resetMenu()
            if UserDefaults.standard.isLoggedIn()
            {
                let vc = ChangePasswordViewController.instantiate(fromAppStoryboard: .Profile)
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.LOGIN_ACCESS_FEATURES)
            }
        }
        else if ConstantManager.appConstant.sideMenuSelectIndex == 5
        {
            resetMenu()
            
            if UserDefaults.standard.isLoggedIn()
            {
                UserDefaults.standard.setLoggedIn(value: false)
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.SUCCESS_LOGOUT)
            }
            else
            {
                let vc = SignInViewController.instantiate(fromAppStoryboard: .Splash)
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            /*
            let vc = SplashNavViewController.instantiate(fromAppStoryboard: .Splash)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)*/
            
            
        }
        else if ConstantManager.appConstant.sideMenuSelectIndex == 6
        {
            resetMenu()
            if UserDefaults.standard.isLoggedIn()
            {
                let vc = AddNewAddressViewController.instantiate(fromAppStoryboard: .Address)
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.LOGIN_ACCESS_FEATURES)
            }
        }
    }
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
    
    func resetMenu()
    {
        ConstantManager.appConstant.sideMenuSelectIndex = 0
    }
}

extension HomeViewController
{
    func getHome()
    {
        let url =  APIList().getUrlString(url: .HOME)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userId": UserDefaults.standard.getUserId()
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.dataArray = data["data"]
                
                self.bannerCollectionView.reloadData()
                self.categoryCollectionView.reloadData()
                
                if self.dataArray["banner"].arrayValue.count > 0
                {
                    if !self.timer.isValid
                    {
                        self.startTimer()
                    }
                }

                
                self.aboutus = data["data"]["aboutUs"]["content"].stringValue
                self.help = data["data"]["aboutUs"]["help"].stringValue
                ConstantManager.appConstant.VAT = data["data"]["aboutUs"]["vat"].stringValue
                self.contactNo = data["data"]["aboutUs"]["contactUs"].stringValue
                
                if data["data"]["cartcount"].intValue != 0
                {
                    self.cartView.isHidden = false
                    self.cartLbl.text = data["data"]["cartcount"].stringValue
                }
                else
                {
                    self.cartView.isHidden = true
                }
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}
