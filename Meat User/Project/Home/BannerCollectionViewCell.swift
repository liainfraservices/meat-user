//
//  BannerCollectionViewCell.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgView: CustomImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.contentMode = .scaleAspectFill
        // Initialization code
    }
    
    class var identifier: String
    {
        return String(describing: self)
    }
      
    class var nib: UINib
    {
        return UINib(nibName: identifier, bundle: nil)
    }

}
