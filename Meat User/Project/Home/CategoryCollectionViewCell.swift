//
//  BannerCollectionViewCell.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgView: CustomImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLbl.font = UIFont.appMediumFontWith(size: 14)
        nameLbl.text = ""
        imgView.contentMode = .scaleAspectFill
        // Initialization code
    }
    
    class var identifier: String
    {
        return String(describing: self)
    }
      
    class var nib: UINib
    {
        return UINib(nibName: identifier, bundle: nil)
    }

}
