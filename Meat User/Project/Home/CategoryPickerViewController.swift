//
//  CategoryPickerViewController.swift
//  Meat User
//
//  Created by Jai on 01/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import SwiftyJSON

enum VehilceAlertType
{
    case single
    case multiple
}

class CategoryPickerViewController: UIViewController
{
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var vehicleDataArray : [JSON] = []
    var didSelectVehicle : (JSON) -> Void = { _ in }
    var vehicleTitle = ""
    
    var vehilceType : VehilceAlertType = .single
    
    var type = 1
    
    @IBOutlet weak var addAtn: UIButton!
    @IBOutlet weak var addBtnHeight: NSLayoutConstraint!

//    var serviceTypeDataArray : [ServiceTypeModalData] = []
//    var selectedServiceTypeDataArray : [ServiceTypeModalData] = []
//
//    var didSelectMultipleVehicle : ([ServiceTypeModalData],[ServiceTypeModalData]) -> Void = { _,_ in }

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        alertView.layer.cornerRadius = 8
        alertView.clipsToBounds = true
        titleLbl.font = UIFont.appMediumFontWith(size: 16)
        addAtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
//        addAtn.setTitle(CONSTANT.ADD, for: .normal)
        titleLbl.text = vehicleTitle
//        tableView?.register(MultipleVehicleAlertCell.nib, forCellReuseIdentifier: MultipleVehicleAlertCell.identifier)
        tableView.separatorStyle = .none
        if self.vehilceType == .single
        {
            addAtn.isHidden = true
            addBtnHeight.constant = 0
        }
    }
    
    @IBAction func closeAtn(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addAtn(_ sender: Any)
    {
//        didSelectMultipleVehicle(selectedServiceTypeDataArray,serviceTypeDataArray)
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension CategoryPickerViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return vehicleDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
//        if self.vehilceType == .single
//        {
        let cell = tableView.dequeueReusableCell(withIdentifier: VehicleAlertTableViewCell.identifier, for: indexPath) as! VehicleAlertTableViewCell
        cell.selectionStyle = .none
        
        if type == 1
        {
            cell.nameLbl.text = "\(vehicleDataArray[indexPath.row]["desc"].stringValue) - (\(vehicleDataArray[indexPath.row]["price"].stringValue) \(CURRENCY_TYPE))"
        }
        else if type == 2
        {
            cell.nameLbl.text = vehicleDataArray[indexPath.row]["desc"].stringValue
        }
        else if type == 3
        {
            cell.nameLbl.text = "\(vehicleDataArray[indexPath.row]["desc"].stringValue) - (\(vehicleDataArray[indexPath.row]["price"].stringValue) \(CURRENCY_TYPE))"
        }
        else if type == 5
        {
            cell.nameLbl.text = vehicleDataArray[indexPath.row]["address"].stringValue
        }
        else
        {
            cell.nameLbl.text = vehicleDataArray[indexPath.row]["desc"].stringValue
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        if self.vehilceType == .single
//        {
            didSelectVehicle(vehicleDataArray[indexPath.row])
            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    
}
