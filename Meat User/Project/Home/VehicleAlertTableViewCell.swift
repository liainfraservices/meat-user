//
//  VehicleAlertTableViewCell.swift
//  GetTaxi Driver
//
//  Created by Pyramidions on 07/05/19.
//  Copyright © 2019 Pyramidions. All rights reserved.
//

import UIKit

class VehicleAlertTableViewCell: UITableViewCell
{
    @IBOutlet weak var nameLbl: UILabel!
    
    class var identifier: String
    {
        return String(describing: self)
    }
    
    class var nib: UINib
    {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLbl.font = UIFont.appMediumFontWith(size: 16)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
