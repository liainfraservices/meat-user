//
//  CategoryViewController.swift
//  Meat User
//
//  Created by Jai on 30/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CategoryViewController: BaseViewController
{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sizeTitleLbl: UILabel!
    @IBOutlet weak var sizeKGLbl: UILabel!
    @IBOutlet weak var chippingTitleLbl: UILabel!
    @IBOutlet weak var mediumLbl: UILabel!
    @IBOutlet weak var quantityTitleLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var kheemaTitleLbl: UILabel!
    @IBOutlet weak var kheemaLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var packingStyleLbl: UILabel!
    @IBOutlet weak var notesTxtView: UITextView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var addToCartBtn: UIButton!
//    var placeholderLabel : UILabel!
    
    @IBOutlet weak var placeholderLabel: UILabel!
    
    let connection = Connection()
    var productID = ""
    
    var sizeID = ""
    var cuttingID = ""
    var packingID = ""
    var khemaID = ""
    var quantity = 1
    
    var khemaPrice = 0
    var productPrice = 0
    
    var dataArray = JSON()
    var params = JSON()
    var goatName = ""
    var goatImage = ""
    var notes = ""

    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        mainView.isHidden = true
        notesTxtView.delegate = self
//        placeholderLabel = UILabel()
        placeholderLabel.text = CONSTANT.NOTES
        placeholderLabel.font = UIFont.appMediumFontWith(size: 14)
//        placeholderLabel.sizeToFit()
//        notesTxtView.addSubview(placeholderLabel)
//        placeholderLabel.frame.origin = CGPoint(x: 5, y: (notesTxtView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !notesTxtView.text.isEmpty
        placeholderLabel.textAlignment = .right

        priceView.addDashedBorder()
        setTextComponents()
        
        getProducts()
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sizeKGAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = dataArray["ProductDetails"].arrayValue
        vc.vehicleTitle = CONSTANT.SIZE
        vc.type = 1
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
            
            self.sizeKGLbl.text = data["desc"].stringValue
            let price = data["price"].intValue
            let totalPrice = (price * self.quantity) + self.khemaPrice
            self.priceLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
            self.sizeID = data["id"].stringValue
            self.productPrice = data["price"].intValue
            self.params["productDetails"].dictionaryObject = data.dictionaryObject!
        }

    }
    
    @IBAction func mediumAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = dataArray["cuttingStyle"].arrayValue
        vc.vehicleTitle = CONSTANT.CHIPPING
        vc.type = 2
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
            
            self.mediumLbl.text = data["desc"].stringValue
            self.cuttingID = data["id"].stringValue
            self.params["packingStyle"].dictionaryObject = data.dictionaryObject!
        }

    }
    
    @IBAction func quantityIncAtn(_ sender: Any)
    {
        if quantity > 1
        {
            quantity = quantity - 1
            quantityLbl.text = "\(quantity)"
            let totalPrice = (productPrice * quantity) + khemaPrice
            priceLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
            params["qty"].stringValue = "\(quantity)"
        }
    }
    
    @IBAction func quantityDecAtn(_ sender: Any)
    {        
        quantity = quantity + 1
        quantityLbl.text = "\(quantity)"
        let totalPrice = (productPrice * quantity) + khemaPrice
        priceLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
        params["qty"].stringValue = "\(quantity)"
    }
    
    @IBAction func kheemaAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = dataArray["kheemaDetails"].arrayValue
        vc.vehicleTitle = CONSTANT.KHEEMA
        vc.type = 3
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
                        
            self.kheemaLbl.text = data["desc"].stringValue
            let price = (self.productPrice * self.quantity) + data["price"].intValue
            self.priceLbl.text = "\(price) \(CURRENCY_TYPE)"
            self.khemaID = data["id"].stringValue
            self.khemaPrice = data["price"].intValue
            self.params["kheemaDetails"].dictionaryObject = data.dictionaryObject!
        }

    }
    
    @IBAction func packingStyleAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = dataArray["packingStyle"].arrayValue
        vc.vehicleTitle = CONSTANT.PACKING_STYLE
        vc.type = 4
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
            
            self.packingStyleLbl.text = data["desc"].stringValue
            self.packingID = data["id"].stringValue
            self.params["packingStyle"].dictionaryObject = data.dictionaryObject!
        }

    }
    
    @IBAction func continueAtn(_ sender: Any)
    {
        if UserDefaults.standard.isLoggedIn()
        {
            /*
            if notesTxtView.text!.isEmpty
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PLEASE_ENTER_NOTES)
            }
            else
            {*/

                params["userId"].stringValue = UserDefaults.standard.getUserId()


                let utf8str = notesTxtView.text!.data(using: .utf8)

                if let base64Encoded = utf8str?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
                {
                    self.params["notes"].stringValue = base64Encoded
                    self.addCart(isDelivery: false)

                    /*
                    print("Encoded: \(base64Encoded)")

                    if let base64Decoded = Data(base64Encoded: base64Encoded, options: Data.Base64DecodingOptions(rawValue: 0))
                    .map({ String(data: $0, encoding: .utf8) }) {
                        // Convert back to a string
                        print("Decoded: \(base64Decoded ?? "")")
                    }*/
                }
                else
                {
                    self.params["notes"].stringValue = ""
                    self.addCart(isDelivery: false)
                }
                
//                self.params["notes"].stringValue = notesTxtView.text!
//                self.addCart(isDelivery: false)
//            }
        }
        else
        {
            self.requestLogin()
        }
    }
    
    func requestLogin()
    {
        let alert = UIAlertController(title: CONSTANT.PLEASE_LOGIN, message: CONSTANT.DO_YOU_WANT_TO_LOGIN,         preferredStyle: UIAlertController.Style.alert)

        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: CONSTANT.PLEASE_LOGIN as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(myMutableString, forKey: "attributedTitle")
        
        var messageMutableString = NSMutableAttributedString()
        messageMutableString = NSMutableAttributedString(string: CONSTANT.DO_YOU_WANT_TO_LOGIN as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(messageMutableString, forKey: "attributedMessage")

        
        alert.addAction(UIAlertAction(title: CONSTANT.CANCEL, style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: CONSTANT.OK,
                                      style: UIAlertAction.Style.default,
                                      handler:
        {
            (_: UIAlertAction!) in
            self.moveLogin()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func moveLogin()
    {
        let vc = SignInViewController.instantiate(fromAppStoryboard: .Splash)
        vc.isFromBooking = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    
    /*{
        if notesTxtView.text!.isEmpty
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PLEASE_ENTER_NOTES)
        }
        else
        {
            self.params["notes"].stringValue = notesTxtView.text!
            self.addCart(isDelivery: true)
        }
    }*/
    /*{
        let vc = DeliveryAddressViewController.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }*/
    
    @IBAction func addToCartAtn(_ sender: Any)
    {
//        if notesTxtView.text!.isEmpty
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PLEASE_ENTER_NOTES)
//        }
//        else
//        {
            self.params["notes"].stringValue = notesTxtView.text ?? ""
            self.addCart(isDelivery: false)
//        }
    }
    
}

extension CategoryViewController
{
    func setTextComponents()
    {
        sizeTitleLbl.text = CONSTANT.SIZE
        chippingTitleLbl.text = CONSTANT.CHIPPING
        quantityTitleLbl.text = CONSTANT.QUANTITY
        kheemaTitleLbl.text = CONSTANT.KHEEMA
        packingStyleLbl.text = CONSTANT.PACKING_STYLE
        notesTxtView.text = ""
        continueBtn.setTitle(CONSTANT.ADD_TO_CART, for: .normal)
        addToCartBtn.setTitle(CONSTANT.ADD_TO_CART, for: .normal)
        
        sizeTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        sizeKGLbl.font = UIFont.appMediumFontWith(size: 14)
        chippingTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        mediumLbl.font = UIFont.appMediumFontWith(size: 14)
        quantityTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        quantityLbl.font = UIFont.appMediumFontWith(size: 14)
        kheemaTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        kheemaLbl.font = UIFont.appMediumFontWith(size: 14)
        priceLbl.font = UIFont.appMediumFontWith(size: 14)
        packingStyleLbl.font = UIFont.appMediumFontWith(size: 14)
        notesTxtView.font = UIFont.appMediumFontWith(size: 14)
        continueBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        addToCartBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        
        sizeKGLbl.text = ""
        mediumLbl.text = ""
        quantityLbl.text = ""
        kheemaLbl.text = ""
        priceLbl.text = ""
        packingStyleLbl.text = ""
        
    }
    
    func addCart(isDelivery: Bool)
    {
        let url =  APIList().getUrlString(url: .ADD_TO_CART)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter = String(describing: params)

        showLottie()

        connection.requestPOSTRawData(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                if isDelivery
                {
                    
                }
                else
                {
                    self.goBack(content: data["message"].stringValue)
                }
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func goBack(content: String)
    {
        let alert = UIAlertController(title: CONSTANT.OOPS, message: content,         preferredStyle: UIAlertController.Style.alert)
        
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: CONSTANT.OOPS as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(myMutableString, forKey: "attributedTitle")
        
        var messageMutableString = NSMutableAttributedString()
        messageMutableString = NSMutableAttributedString(string: content as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(messageMutableString, forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: CONSTANT.OK,
                                      style: UIAlertAction.Style.default,
                                      handler:
            {
                (_: UIAlertAction!) in
                self.getBack()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getBack()
    {
        self.navigationController?.popViewController(animated: true)
    }

    func getProducts()
    {
        let url =  APIList().getUrlString(url: .PRODUCT_DETAILS)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "product": self.productID,
            "branch": ConstantManager.appConstant.branchID ?? 0
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlert(msg: CONSTANT.NO_PRODUCT)
//                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.NO_PRODUCT)
            }
            else
            {
                if (data["data"].dictionary != nil)
                {
                    self.setValue(data: data["data"])
                }
                else
                {
                    self.showAlert(msg: data["message"].stringValue)
//                    self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
                }
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func showAlert(msg: String)
    {
        let alert = UIAlertController(title: CONSTANT.OOPS, message: msg,         preferredStyle: UIAlertController.Style.alert)

        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: CONSTANT.OOPS as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(myMutableString, forKey: "attributedTitle")
        
        var messageMutableString = NSMutableAttributedString()
        messageMutableString = NSMutableAttributedString(string: msg as String, attributes: [NSAttributedString.Key.font:UIFont.appMediumFontWith(size: 14)])
        alert.setValue(messageMutableString, forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: CONSTANT.OK,
                                      style: UIAlertAction.Style.default,
                                      handler:
        {
            (_: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setValue(data: JSON)
    {
        dataArray = data
        mainView.isHidden = false
        
        params["goatImage"].stringValue = self.goatImage
        params["goatName"].stringValue = self.goatName

        params["userId"].stringValue = UserDefaults.standard.getUserId()
        params["productId"].stringValue = data["productId"].stringValue
        params["qty"].stringValue = "1"
        params["branchId"].stringValue = ConstantManager.appConstant.branchID ?? "0"
        params["branchName"].stringValue = ConstantManager.appConstant.branchName

        var price = 0
        
        quantityLbl.text = "\(quantity)"
        
        if let productDetails = data["ProductDetails"].array
        {
            if productDetails.count > 0
            {
                sizeKGLbl.text = productDetails[0]["desc"].stringValue
                price = productDetails[0]["price"].intValue
                priceLbl.text = "\(productDetails[0]["price"].stringValue) \(CURRENCY_TYPE)"
                sizeID = productDetails[0]["id"].stringValue
                productPrice = productDetails[0]["price"].intValue
                
//                let jsonData = try! JSONSerialization.data(withJSONObject: productDetails[0].dictionaryObject!, options: [])
//                let decoded = String(data: jsonData, encoding: .utf8)!
                params["productDetails"].dictionaryObject = productDetails[0].dictionaryObject!
            }
        }
        
        if let productDetails = data["cuttingStyle"].array
        {
            if productDetails.count > 0
            {
                mediumLbl.text = productDetails[0]["desc"].stringValue
                cuttingID = productDetails[0]["id"].stringValue
//                let jsonData = try! JSONSerialization.data(withJSONObject: productDetails[0].dictionaryObject!, options: [])
//                let decoded = String(data: jsonData, encoding: .utf8)!
                params["cuttingStyle"].dictionaryObject = productDetails[0].dictionaryObject!
            }
        }
        
        if let productDetails = data["packingStyle"].array
        {
            if productDetails.count > 0
            {
                packingStyleLbl.text = productDetails[0]["desc"].stringValue
                packingID = productDetails[0]["id"].stringValue
//                let jsonData = try! JSONSerialization.data(withJSONObject: productDetails[0].dictionaryObject!, options: [])
//                let decoded = String(data: jsonData, encoding: .utf8)!
                params["packingStyle"].dictionaryObject = productDetails[0].dictionaryObject!
            }
        }

        if let productDetails = data["kheemaDetails"].array
        {
            if productDetails.count > 0
            {
                kheemaLbl.text = productDetails[0]["desc"].stringValue
                let price = price + productDetails[0]["price"].intValue
                priceLbl.text = "\(price) \(CURRENCY_TYPE)"
                khemaID = productDetails[0]["id"].stringValue
                khemaPrice = productDetails[0]["price"].intValue
//                let jsonData = try! JSONSerialization.data(withJSONObject: productDetails[0].dictionaryObject!, options: [])
//                let decoded = String(data: jsonData, encoding: .utf8)!
                params["kheemaDetails"].dictionaryObject = productDetails[0].dictionaryObject!
            }
        }
        
        print("Params = ",String(describing: params))
        
    }
}

extension CategoryViewController: UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
