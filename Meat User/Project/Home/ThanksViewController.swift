//
//  ThanksViewController.swift
//  Meat User
//
//  Created by Jai on 04/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

protocol ThanksConsultDelegate: class
{
    func didThanksConsult()
}

class ThanksViewController: UIViewController {

    @IBOutlet weak var thanksTitleLbl: UILabel!
    @IBOutlet weak var appNoLbl: UILabel!
    @IBOutlet weak var confirmedLbl: UILabel!
    weak var delegate: ThanksConsultDelegate?
    var appNo = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setComponents()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)

        DispatchQueue.main.asyncAfter(deadline: .now() + 2)
        {
            self.dismiss(animated: true, completion: {
                self.delegate?.didThanksConsult()
            })
        }

    }

}

extension ThanksViewController
{
    func setComponents()
    {
        thanksTitleLbl.font = UIFont.appMediumFontWith(size: 14)
        appNoLbl.font = UIFont.appMediumFontWith(size: 14)
        confirmedLbl.font = UIFont.appMediumFontWith(size: 14)

        thanksTitleLbl.text = CONSTANT.THANK_YOU
        appNoLbl.text = "\(CONSTANT.APPLICATION_NO) \(self.appNo)"
        confirmedLbl.text = CONSTANT.ORDER_CONFIRMED
    }
}
