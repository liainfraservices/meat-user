//
//  MenuViewController.swift
//  Meat User
//
//  Created by Jai on 04/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var dashboardBtn: UIButton!
    @IBOutlet weak var termsBtn: UIButton!
    @IBOutlet weak var helpBtn: UIButton!
    @IBOutlet weak var changePasswordBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var addAddressBtn: UIButton!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setComponents()
    }
    
    @IBAction func dashboardAtn(_ sender: Any)
    {
        ConstantManager.appConstant.sideMenuSelectIndex = 1
        close()
    }
    
    @IBAction func termsAtn(_ sender: Any)
    {
        ConstantManager.appConstant.sideMenuSelectIndex = 2
        close()
    }
    
    @IBAction func helpAtn(_ sender: Any)
    {
        ConstantManager.appConstant.sideMenuSelectIndex = 3
        close()
    }
    
    @IBAction func changePasswordAtn(_ sender: Any)
    {
        ConstantManager.appConstant.sideMenuSelectIndex = 4
        close()
    }
    
    @IBAction func logoutAtn(_ sender: Any)
    {
        ConstantManager.appConstant.sideMenuSelectIndex = 5
        close()
    }
    
    @IBAction func addAddressAtn(_ sender: Any)
    {
        ConstantManager.appConstant.sideMenuSelectIndex = 6
        close()
    }
    
    func close()
    {
        dismiss(animated: true, completion: nil)
    }
    
}

extension MenuViewController
{
    func setComponents()
    {
        dashboardBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        termsBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        helpBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        changePasswordBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        logoutBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        addAddressBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        
        dashboardBtn.setTitle(CONSTANT.DASHBOARD, for: .normal)
        termsBtn.setTitle(CONSTANT.TERMS_AND_COND, for: .normal)
        helpBtn.setTitle(CONSTANT.HELP_CONTACT, for: .normal)
        changePasswordBtn.setTitle(CONSTANT.CHANGE_PASSWORD, for: .normal)
        addAddressBtn.setTitle(CONSTANT.ADD_ADDRESS, for: .normal)
        
        if UserDefaults.standard.isLoggedIn()
        {
            logoutBtn.setTitle(CONSTANT.LOGOUT, for: .normal)
        }
        else
        {
            logoutBtn.setTitle(CONSTANT.LOGIN, for: .normal)
        }
    }
}
