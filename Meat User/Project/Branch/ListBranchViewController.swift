//
//  ListBranchViewController.swift
//  Meat User
//
//  Created by Jai on 09/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ListBranchViewController: BaseViewController
{

    let connection = Connection()
    @IBOutlet weak var tableView: UITableView!
    var dataArray: [JSON] = []
    var selectionData: [JSON] = []
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    var isFromSplash = false
    
    @IBOutlet weak var desLbl: UILabel!
    @IBOutlet weak var selectionLbl: UILabel!
    @IBOutlet weak var continueLbl: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView?.register(ListBranchTableViewCell.nib, forCellReuseIdentifier: ListBranchTableViewCell.identifier)
        tableView.separatorStyle = .none
//        tableView.dataSource = self
//        tableView.delegate = self
        
        desLbl.text = CONSTANT.BARNCH_DES
        selectionLbl.text = CONSTANT.PLEASE_SELECT
        continueLbl.text = CONSTANT.CONTINUE

        
        if isFromSplash
        {
            backView.isHidden = true
            getListBranch()
        }
        else
        {
            if dataArray.count > 0
            {
                selectionLbl.text = dataArray[0]["address"].stringValue
                selectionData.append(dataArray[0])
            }
        }
        titleLbl.font = UIFont.appMediumFontWith(size: 14)
        titleLbl.text = CONSTANT.SELECT_BRANCH_TITLE
        
        desLbl.font = UIFont.appMediumFontWith(size: 14)
        selectionLbl.font = UIFont.appMediumFontWith(size: 14)
        continueLbl.font = UIFont.appMediumFontWith(size: 14)

        
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectionAtn(_ sender: Any)
    {
        let vc = CategoryPickerViewController.instantiate(fromAppStoryboard: .Home)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.vehicleDataArray = self.dataArray
        vc.vehicleTitle = CONSTANT.SELECT_BRANCH_TITLE
        vc.type = 5
        self.present(vc, animated: true, completion: nil)
        
        vc.didSelectVehicle =
        {
            data in
            self.selectionData.removeAll()
            self.selectionData.append(data)
            self.selectionLbl.text = data["address"].stringValue
        }

    }
        
    @IBAction func continueAtn(_ sender: Any)
    {
        if selectionData.count > 0
        {
            if isFromSplash
            {
                UserDefaults.standard.setBranch(value: selectionData[0])
                let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
                vc.modalPresentationStyle = .fullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
            else
            {
                UserDefaults.standard.setBranch(value: selectionData[0])
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    

}

extension ListBranchViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = dataArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ListBranchTableViewCell.identifier, for: indexPath) as! ListBranchTableViewCell
        cell.branchNameLbl.text = item["address"].stringValue
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*
        if isFromSplash
        {
            UserDefaults.standard.setBranch(value: dataArray[indexPath.row])
            let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
        else
        {
            UserDefaults.standard.setBranch(value: dataArray[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        }*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
}

extension ListBranchViewController
{
    func getListBranch()
    {
        let url =  APIList().getUrlString(url: .LIST_BRANCHES)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userId": UserDefaults.standard.getUserId()
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.dataArray = data["data"]["branches"].arrayValue
                
                if self.dataArray.count > 0
                {
                    self.selectionLbl.text = self.dataArray[0]["address"].stringValue
                    self.selectionData.append(self.dataArray[0])
                }

                
//                self.tableView.reloadData()
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}

extension ListBranchViewController: ListBranchTableViewDelegate
{
    func didSelectListBranch(cell: ListBranchTableViewCell)
    {
        if let indexPath = tableView.indexPath(for: cell)
        {
            if isFromSplash
            {
                UserDefaults.standard.setBranch(value: dataArray[indexPath.row])
                let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
                vc.modalPresentationStyle = .fullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
            else
            {
                UserDefaults.standard.setBranch(value: dataArray[indexPath.row])
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
