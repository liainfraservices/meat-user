//
//  ListBranchTableViewCell.swift
//  Meat User
//
//  Created by Jai on 09/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

protocol ListBranchTableViewDelegate: class
{
    func didSelectListBranch(cell: ListBranchTableViewCell)
}

class ListBranchTableViewCell: UITableViewCell {

    @IBOutlet weak var branchNameLbl: UILabel!
    weak var delegate: ListBranchTableViewDelegate?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        branchNameLbl.font = UIFont.appMediumFontWith(size: 14)
        branchNameLbl.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class var identifier: String
    {
        return String(describing: self)
    }
      
    class var nib: UINib
    {
        return UINib(nibName: identifier, bundle: nil)
    }

    @IBAction func selectBranchAtn(_ sender: Any)
    {
        self.delegate?.didSelectListBranch(cell: self)
    }
    
    
}
