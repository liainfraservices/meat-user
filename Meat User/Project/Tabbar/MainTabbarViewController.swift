//
//  MainTabbarViewController.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

class MainTabbarViewController: UITabBarController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.delegate = self
    }
}

extension MainTabbarViewController: UITabBarControllerDelegate
{
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        if !UserDefaults.standard.isLoggedIn()
        {
            if tabBarController.selectedIndex == 1 || tabBarController.selectedIndex == 2 || tabBarController.selectedIndex == 3
            {
                tabBarController.selectedIndex = 0
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.LOGIN_ACCESS_FEATURES)
            }
        }
    }
}

