//
//  CartTableViewCell.swift
//  Meat User
//
//  Created by Jai on 01/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

class CurrentBookingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var goatNameLbl: UILabel!
    @IBOutlet weak var sizeTitleLbl: UILabel!
    @IBOutlet weak var sizeLbl: UILabel!
    @IBOutlet weak var chippingTitleLbl: UILabel!
    @IBOutlet weak var chippingLbl: UILabel!
    @IBOutlet weak var quantityTitleLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var mincedMeatTitleLbl: UILabel!
    @IBOutlet weak var mincedMeatLbl: UILabel!
    @IBOutlet weak var encapsulationTitleLbl: UILabel!
    @IBOutlet weak var encapsulationLbl: UILabel!
    @IBOutlet weak var cityTitleLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var valueTitleLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var vatTitleLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!

    
    @IBOutlet weak var nameTitleLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var mobileNoTitleLbl: UILabel!
    @IBOutlet weak var mobileNoLbl: UILabel!
    @IBOutlet weak var orderStatusTitleLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    
    
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var orderStateLbl: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTextComponents()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class var identifier: String
    {
        return String(describing: self)
    }
    
    class var nib: UINib
    {
        return UINib(nibName: identifier, bundle: nil)
    }
    
}

extension CurrentBookingTableViewCell
{
    func setTextComponents()
    {
        deleteBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        goatNameLbl.font = UIFont.appRegularFontWith(size: 14)
        sizeTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        sizeLbl.font = UIFont.appRegularFontWith(size: 12)
        chippingTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        chippingLbl.font = UIFont.appRegularFontWith(size: 12)
        quantityTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        quantityLbl.font = UIFont.appRegularFontWith(size: 12)
        mincedMeatTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        mincedMeatLbl.font = UIFont.appRegularFontWith(size: 12)
        encapsulationTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        encapsulationLbl.font = UIFont.appRegularFontWith(size: 12)
        cityTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        cityLbl.font = UIFont.appRegularFontWith(size: 12)
        valueTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        valueLbl.font = UIFont.appRegularFontWith(size: 12)
        vatTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        vatLbl.font = UIFont.appRegularFontWith(size: 12)
        totalTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        totalLbl.font = UIFont.appRegularFontWith(size: 12)

        deleteBtn.setTitle(CONSTANT.DELETE, for: .normal)
        sizeTitleLbl.text = CONSTANT.SIZE
        chippingTitleLbl.text = CONSTANT.CHIPPING
        quantityTitleLbl.text = CONSTANT.QUANTITY
        mincedMeatTitleLbl.text = CONSTANT.MINCED_MEAT
        encapsulationTitleLbl.text = CONSTANT.ENCAPSULATION
        cityTitleLbl.text = CONSTANT.CITY
        valueTitleLbl.text = CONSTANT.VALUE
        vatTitleLbl.text = CONSTANT.VAT
        totalTitleLbl.text = CONSTANT.TOTAL
        
        nameTitleLbl.text = CONSTANT.ORDER_ID
        nameLbl.text = ""
        mobileNoTitleLbl.text = CONSTANT.ADDRESS
        mobileNoLbl.text = ""
        orderStatusTitleLbl.text = CONSTANT.MOBILE_NO
        orderStatusLbl.text = ""
        
        nameTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        nameLbl.font = UIFont.appMediumFontWith(size: 12)
        mobileNoTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        mobileNoLbl.font = UIFont.appMediumFontWith(size: 12)
        orderStatusTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        orderStatusLbl.font = UIFont.appMediumFontWith(size: 12)
        orderDateLbl.font = UIFont.appMediumFontWith(size: 12)
        orderStateLbl.font = UIFont.appMediumFontWith(size: 12)
    }
}
