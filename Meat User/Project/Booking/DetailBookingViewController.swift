//
//  DetailBookingViewController.swift
//  Meat User
//
//  Created by Jai on 15/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DetailBookingViewController: UIViewController
{
    
    @IBOutlet weak var tableView: UITableView!
    var dataArray: [JSON] = []

    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView?.register(CartTableViewCell.nib, forCellReuseIdentifier: CartTableViewCell.identifier)
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

}

extension DetailBookingViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = dataArray[indexPath.row]["details"]
        
        
        print("dataArray = ",item)

        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.identifier, for: indexPath) as! CartTableViewCell
        
        let quantity = item["qty"].intValue
        let khemmaPrice = item["kheemaDetails"]["price"].intValue
        let price = item["productDetails"]["price"].intValue
//        let totalPrice = (price * quantity) + khemmaPrice + Int(ConstantManager.appConstant.VAT)!
  
        let totalPrice = (price * quantity) + khemmaPrice
 
        ImageLoader().imageLoad(imgView: cell.imgView, url: item["goatImage"].stringValue)
        cell.goatNameLbl.text = item["goatName"].stringValue
        cell.sizeLbl.text = item["productDetails"]["desc"].stringValue
        cell.chippingLbl.text = item["cuttingStyle"]["desc"].stringValue
        cell.quantityLbl.text = "\(quantity)"
        cell.cartQuantityLbl.text = "\(quantity)"
        cell.mincedMeatLbl.text = item["kheemaDetails"]["desc"].stringValue
        cell.valueLbl.text = "\(price * quantity) \(CURRENCY_TYPE)"
        cell.vatLbl.text = "\(ConstantManager.appConstant.VAT) \(CURRENCY_TYPE)"
        cell.totalLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
        cell.cityLbl.text = item["branchName"].stringValue
        
        cell.deleteView.isHidden = true
        cell.incDecView.isHidden = true
        
//        cell.delegate = self
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*
        let item = dataArray[indexPath.row]["details"][0]
        let vc = UpdateCategoryViewController.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.productID = item["productId"].stringValue
        vc.goatName = item["goatName"].stringValue
        vc.goatImage = item["goatImage"].stringValue
        vc.dataArray = item
        vc.cartid = dataArray[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}
