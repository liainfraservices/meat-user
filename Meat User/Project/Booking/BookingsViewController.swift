//
//  BookingsViewController.swift
//  Meat User
//
//  Created by Jai on 02/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class BookingsViewController: BaseViewController
{

    @IBOutlet weak var tableView: UITableView!
    var dataArray: [JSON] = []
    let connection = Connection()
    
    @IBOutlet weak var noBookingsView: UIView!
    @IBOutlet weak var noBookingLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noBookingsView.isHidden = true
        tableView?.register(CurrentBookingTableViewCell.nib, forCellReuseIdentifier: CurrentBookingTableViewCell.identifier)
        tableView.separatorStyle = .none
//        tableView.dataSource = self
//        tableView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        listOrder()
    }
    
}

extension BookingsViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if dataArray.count > 0
        {
            noBookingsView.isHidden = true
        }
        else
        {
            noBookingsView.isHidden = false
        }
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = self.dataArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrentBookingTableViewCell.identifier, for: indexPath) as! CurrentBookingTableViewCell
        cell.nameLbl.text = item["applicationOrderId"].stringValue
        cell.mobileNoLbl.text = item["addressdetails"]["address"].stringValue
        cell.orderStatusLbl.text = item["addressdetails"]["phone"].stringValue
        cell.orderDateLbl.text = item["orderDate"].stringValue
        cell.orderStateLbl.text = item["orderStatus"].stringValue

        let VAT = item["vat"].stringValue
        
        var totalPrice = 0
        for (index, element) in self.dataArray[indexPath.row]["orderdetails"].arrayValue.enumerated()
        {
            let item = element["details"]
            let quantity = item["qty"].intValue
            let khemmaPrice = item["kheemaDetails"]["price"].intValue
            let price = item["productDetails"]["price"].intValue
            totalPrice = ((price * quantity) + khemmaPrice) + totalPrice
//            self.totalPrice = self.totalPrice + totalPrice
        }
        
        let value = "\(totalPrice) \(CURRENCY_TYPE)"
        cell.valueLbl.text = value
        cell.vatLbl.text = "\(VAT) %"
        
        let vat = (Double(VAT) ?? 5.0 / 100.0)
        cell.totalLbl.text = "\((Double(totalPrice) * vat) + Double(totalPrice)) \(CURRENCY_TYPE)"

        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = DetailBookingViewController.instantiate(fromAppStoryboard: .Order)
        vc.dataArray = self.dataArray[indexPath.row]["orderdetails"].arrayValue
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}

extension BookingsViewController
{
    func listOrder()
    {
        let url =  APIList().getUrlString(url: .LIST_ORDER)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userId": UserDefaults.standard.getUserId()
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                if self.tableView.delegate == nil
                {
                    self.tableView.dataSource = self
                    self.tableView.delegate = self
                }

                self.tableView.reloadData()

//                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                if self.tableView.delegate == nil
                {
                    self.tableView.dataSource = self
                    self.tableView.delegate = self
                }
                
                self.dataArray = data["data"].arrayValue
                self.tableView.reloadData()
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}

