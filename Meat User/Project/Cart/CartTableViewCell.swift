//
//  CartTableViewCell.swift
//  Meat User
//
//  Created by Jai on 01/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit

protocol CartTableViewCellDelegate: class
{
    func didDeleteCart(cell: CartTableViewCell)
    func didIncreaseCart(cell: CartTableViewCell)
    func didDecreaseCart(cell: CartTableViewCell)

}


class CartTableViewCell: UITableViewCell {

    
    weak var delegate: CartTableViewCellDelegate?
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var goatNameLbl: UILabel!
    @IBOutlet weak var sizeTitleLbl: UILabel!
    @IBOutlet weak var sizeLbl: UILabel!
    @IBOutlet weak var chippingTitleLbl: UILabel!
    @IBOutlet weak var chippingLbl: UILabel!
    @IBOutlet weak var quantityTitleLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var mincedMeatTitleLbl: UILabel!
    @IBOutlet weak var mincedMeatLbl: UILabel!
    @IBOutlet weak var encapsulationTitleLbl: UILabel!
    @IBOutlet weak var encapsulationLbl: UILabel!
    @IBOutlet weak var cityTitleLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var valueTitleLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var vatTitleLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var cartQuantityLbl: UILabel!
    
    
    @IBOutlet weak var incDecView: CustomView!
    @IBOutlet weak var deleteView: CustomView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTextComponents()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class var identifier: String
    {
        return String(describing: self)
    }
    
    class var nib: UINib
    {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    @IBAction func deleteCartAtn(_ sender: Any)
    {
        self.delegate?.didDeleteCart(cell: self)
    }
    
    @IBAction func increaseAtn(_ sender: Any)
    {
        self.delegate?.didIncreaseCart(cell: self)
    }
    
    
    @IBAction func decreaseAtn(_ sender: Any)
    {
        self.delegate?.didDecreaseCart(cell: self)
    }
    
    
}

extension CartTableViewCell
{
    func setTextComponents()
    {
        deleteBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        goatNameLbl.font = UIFont.appRegularFontWith(size: 14)
        sizeTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        sizeLbl.font = UIFont.appRegularFontWith(size: 12)
        chippingTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        chippingLbl.font = UIFont.appRegularFontWith(size: 12)
        quantityTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        quantityLbl.font = UIFont.appRegularFontWith(size: 12)
        mincedMeatTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        mincedMeatLbl.font = UIFont.appRegularFontWith(size: 12)
        encapsulationTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        encapsulationLbl.font = UIFont.appRegularFontWith(size: 12)
        cityTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        cityLbl.font = UIFont.appRegularFontWith(size: 12)
        valueTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        valueLbl.font = UIFont.appRegularFontWith(size: 12)
        vatTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        vatLbl.font = UIFont.appRegularFontWith(size: 12)
        totalTitleLbl.font = UIFont.appRegularFontWith(size: 12)
        totalLbl.font = UIFont.appRegularFontWith(size: 12)        
        cartQuantityLbl.font = UIFont.appMediumFontWith(size: 12)

        deleteBtn.setTitle(CONSTANT.DELETE, for: .normal)
        sizeTitleLbl.text = CONSTANT.SIZE
        chippingTitleLbl.text = CONSTANT.CHIPPING
        quantityTitleLbl.text = CONSTANT.QUANTITY
        mincedMeatTitleLbl.text = CONSTANT.KHEEMA
        encapsulationTitleLbl.text = CONSTANT.PACKING_STYLE
        cityTitleLbl.text = CONSTANT.BRANCH
        valueTitleLbl.text = CONSTANT.VALUE
        vatTitleLbl.text = CONSTANT.VAT
        totalTitleLbl.text = CONSTANT.TOTAL
    }
}
