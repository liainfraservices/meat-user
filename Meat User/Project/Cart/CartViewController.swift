//
//  CartViewController.swift
//  Meat User
//
//  Created by Jai on 01/07/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

// ELC F circuit

class CartViewController: BaseViewController
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var followUpBtn: UIButton!
    
    let connection = Connection()
    var dataArray: [JSON] = []
    
    @IBOutlet weak var noCartView: UIView!
    @IBOutlet weak var noCartLbl: UILabel!
    
    
    var totalPrice = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        noCartLbl.text = "لم يتم العثور على منتجات في سلة التسوق";
        noCartView.isHidden = true
        
        followUpBtn.isHidden = true
        
        followUpBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        followUpBtn.setTitle(CONSTANT.FOLLOWUP, for: .normal)
        tableView?.register(CartTableViewCell.nib, forCellReuseIdentifier: CartTableViewCell.identifier)
        tableView.separatorStyle = .none
//        tableView.dataSource = self
//        tableView.delegate = self
    }
        
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        getCart()
    }
    
    @IBAction func followUpAtn(_ sender: Any)
    {
        let vc = ListAddressViewController.instantiate(fromAppStoryboard: .Address)
        vc.hidesBottomBarWhenPushed = true
        vc.cartArray = self.dataArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CartViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if dataArray.count > 0
        {
            noCartView.isHidden = true
        }
        else
        {
            noCartView.isHidden = false
        }
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = dataArray[indexPath.row]["details"][0]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.identifier, for: indexPath) as! CartTableViewCell
        
        let quantity = item["qty"].intValue
        let khemmaPrice = item["kheemaDetails"]["price"].intValue
        let price = item["productDetails"]["price"].intValue
//        let totalPrice = (price * quantity) + khemmaPrice + Int(ConstantManager.appConstant.VAT)!
  
        let totalPrice = (price * quantity) + khemmaPrice
 
        ImageLoader().imageLoad(imgView: cell.imgView, url: item["goatImage"].stringValue)
        cell.goatNameLbl.text = item["goatName"].stringValue
        cell.sizeLbl.text = item["productDetails"]["desc"].stringValue
        cell.chippingLbl.text = item["cuttingStyle"]["desc"].stringValue
        cell.quantityLbl.text = "\(quantity)"
        cell.cartQuantityLbl.text = "\(quantity)"
        cell.mincedMeatLbl.text = item["kheemaDetails"]["desc"].stringValue
        cell.valueLbl.text = "\(price * quantity) \(CURRENCY_TYPE)"
        cell.vatLbl.text = "\(ConstantManager.appConstant.VAT) \(CURRENCY_TYPE)"
        cell.totalLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
        cell.cityLbl.text = item["branchName"].stringValue
        
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*
        let item = dataArray[indexPath.row]["details"][0]
        let vc = UpdateCategoryViewController.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.productID = item["productId"].stringValue
        vc.goatName = item["goatName"].stringValue
        vc.goatImage = item["goatImage"].stringValue
        vc.dataArray = item
        vc.cartid = dataArray[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}

extension CartViewController
{
    func getCart()
    {
        let url =  APIList().getUrlString(url: .LIST_CART)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userId": UserDefaults.standard.getUserId()
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                if self.tableView.delegate == nil
                {
                    self.tableView.dataSource = self
                    self.tableView.delegate = self
                }

                self.dataArray.removeAll()
                self.tableView.reloadData()
                self.followUpBtn.isHidden = true

//                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                
                if self.tableView.delegate == nil
                {
                    self.tableView.dataSource = self
                    self.tableView.delegate = self
                }

                
                self.dataArray = data["data"].arrayValue
                
                if data["data"].arrayValue.count > 0
                {
                    self.followUpBtn.isHidden = false
                }
                
                self.totalPrice = 0
                
                for (index, element) in self.dataArray.enumerated()
                {
                    let item = element["details"][0]
                    let quantity = item["qty"].intValue
                    let khemmaPrice = item["kheemaDetails"]["price"].intValue
                    let price = item["productDetails"]["price"].intValue
                    let totalPrice = (price * quantity) + khemmaPrice
                    self.totalPrice = self.totalPrice + totalPrice
                }
                
                let value = "\(CONSTANT.FOLLOWUP)  \(self.totalPrice) \(CURRENCY_TYPE)"
                self.followUpBtn.setTitle(value, for: .normal)
                self.tableView.reloadData()
            }
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func deleteCart(cartid: String,index: IndexPath)
    {
        let url =  APIList().getUrlString(url: .DELETE_CART)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
//        let parameter : Parameters = [
//            "userId": UserDefaults.standard.getUserId(),
//            "cartid": cartid
//        ]
        
        let parameter : JSON = [
            "userid":UserDefaults.standard.getUserId(),
            "cartid": cartid
        ]

        showLottie()

        connection.requestPOSTRawData(url, params: String(describing: parameter), headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.dataArray.remove(at: index.row)
                self.tableView.deleteRows(at: [index], with: .left)
                self.tableView.reloadData()
                
                if self.dataArray.count > 0
                {
                    self.followUpBtn.isHidden = false
                }
                else
                {
                    self.followUpBtn.isHidden = true
                }
                
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func addCart(cartid: String, qty: String)
    {
        let url =  APIList().getUrlString(url: .UPDATE_CART)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter: Parameters = [
            "cartid": cartid,
            "userid": UserDefaults.standard.getUserId(),
            "qty": qty
        ]
        

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                
            }
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}

extension CartViewController: CartTableViewCellDelegate
{
    func didIncreaseCart(cell: CartTableViewCell)
    {
        if let indexPath = tableView.indexPath(for: cell)
        {
            let item = dataArray[indexPath.row]["details"][0]
            let quantity = item["qty"].intValue + 1
            cell.cartQuantityLbl.text = "\(quantity)"
            dataArray[indexPath.row]["details"][0]["qty"].stringValue = "\(quantity)"
            let khemmaPrice = item["kheemaDetails"]["price"].intValue
            let price = item["productDetails"]["price"].intValue
            let totalPrice = (price * quantity) + khemmaPrice
            cell.totalLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
            cell.quantityLbl.text = "\(quantity)"
            self.addCart(cartid: dataArray[indexPath.row]["id"].stringValue, qty: "\(quantity)")
            
            self.totalPrice = (price + self.totalPrice)
            
            let value = "\(CONSTANT.FOLLOWUP)  \(self.totalPrice) \(CURRENCY_TYPE)"
            self.followUpBtn.setTitle(value, for: .normal)

        }
    }
    
    func didDecreaseCart(cell: CartTableViewCell)
    {
        if let indexPath = tableView.indexPath(for: cell)
        {
            let item = dataArray[indexPath.row]["details"][0]
            var quantity = item["qty"].intValue
            if quantity > 1
            {
                quantity = quantity - 1
                cell.cartQuantityLbl.text = "\(quantity)"
                dataArray[indexPath.row]["details"][0]["qty"].stringValue = "\(quantity)"
                let khemmaPrice = item["kheemaDetails"]["price"].intValue
                let price = item["productDetails"]["price"].intValue
                let totalPrice = (price * quantity) + khemmaPrice
                cell.totalLbl.text = "\(totalPrice) \(CURRENCY_TYPE)"
                cell.quantityLbl.text = "\(quantity)"
                self.addCart(cartid: dataArray[indexPath.row]["id"].stringValue, qty: "\(quantity)")
                
                /*
                for (index, element) in self.dataArray.enumerated()
                {
                    let item = element["details"][0]
                    let quantity = item["qty"].intValue
                    let khemmaPrice = item["kheemaDetails"]["price"].intValue
                    let price = item["productDetails"]["price"].intValue
                    let totalPrice = (price * quantity) + khemmaPrice
                    self.totalPrice = self.totalPrice + totalPrice
                }*/
                
                self.totalPrice = (self.totalPrice - (price))

                let value = "\(CONSTANT.FOLLOWUP)  \(self.totalPrice) \(CURRENCY_TYPE)"
                self.followUpBtn.setTitle(value, for: .normal)
            }
        }
    }
    
    func didDeleteCart(cell: CartTableViewCell)
    {
        if let indexPath = tableView.indexPath(for: cell)
        {
            self.deleteCart(cartid: dataArray[indexPath.row]["id"].stringValue, index: indexPath)
        }
    }
}
