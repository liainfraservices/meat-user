//
//  ProfileViewController.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditProfileViewController: BaseViewController {

    
    @IBOutlet weak var registerTitleLbl: UILabel!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var mobileNoTxt: UITextField!
    @IBOutlet weak var alreadyAccountBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
    let connection = Connection()
    var dataArray = JSON()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setComponents()
        signUpBtn.isHidden = false
        
        nameTxt.text = dataArray["data"]["custName"].stringValue
        emailTxt.text = dataArray["data"]["custEmail"].stringValue
        mobileNoTxt.text = dataArray["data"]["custPhone"].stringValue

        
        // Do any additional setup after loading the view.
    }
    @IBAction func alreadyHaveAtn(_ sender: Any)
    {
//        let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
//        vc.modalPresentationStyle = .fullScreen
//        vc.modalTransitionStyle = .crossDissolve
//        self.present(vc, animated: true, completion: nil)

//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpAtn(_ sender: Any)
    {
        if nameTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_NAME_REGISTER)
        }
        else if emailTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_EMAIL)
        }
        else if mobileNoTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_MOBILE)
        }
//        else if passwordTxt.text?.isEmpty ?? true
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_PASSWORD)
//        }
//        else if confirmPasswordTxt.text?.isEmpty ?? true
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_CONFIRM_PASSWORD)
//        }
//        else if passwordTxt.text == confirmPasswordTxt.text
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PASSWORD_NOT_SAME)
//        }
        else
        {
            self.updateProfile()
        }
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}

extension EditProfileViewController
{
    func setComponents()
    {
        registerTitleLbl.font = UIFont.appMediumFontWith(size: 16)
        nameTxt.font = UIFont.appRegularFontWith(size: 14)
        emailTxt.font = UIFont.appRegularFontWith(size: 14)
        mobileNoTxt.font = UIFont.appRegularFontWith(size: 14)
//        passwordTxt.font = UIFont.appRegularFontWith(size: 14)
//        confirmPasswordTxt.font = UIFont.appRegularFontWith(size: 14)
        alreadyAccountBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        signUpBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        
        registerTitleLbl.text = CONSTANT.EDIT_PROFILE
        nameTxt.placeholder = CONSTANT.NAME
        emailTxt.placeholder = CONSTANT.EMAIL
        mobileNoTxt.placeholder = CONSTANT.MOBILE_NO
//        passwordTxt.placeholder = CONSTANT.CURRENT_PASSWORD
//        confirmPasswordTxt.placeholder = CONSTANT.NEW_PASSWORD
        alreadyAccountBtn.setTitle(CONSTANT.LOGOUT, for: .normal)
        signUpBtn.setTitle(CONSTANT.SAVE, for: .normal)

        
        mobileNoTxt.keyboardType = .phonePad
    }
    
    func updateProfile()
    {
        let url =  APIList().getUrlString(url: .UPDATEPROFILE)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userId": UserDefaults.standard.getUserId(),
            "name":nameTxt.text!,
            "mobile": mobileNoTxt.text!,
            "email": emailTxt.text!
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}
