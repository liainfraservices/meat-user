//
//  ProfileViewController.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChangePasswordViewController: BaseViewController {

    
    @IBOutlet weak var registerTitleLbl: UILabel!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var mobileNoTxt: UITextField!
    @IBOutlet weak var alreadyAccountBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
    let connection = Connection()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setComponents()
        signUpBtn.isHidden = false
    }
    @IBAction func alreadyHaveAtn(_ sender: Any)
    {
//        let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
//        vc.modalPresentationStyle = .fullScreen
//        vc.modalTransitionStyle = .crossDissolve
//        self.present(vc, animated: true, completion: nil)

//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpAtn(_ sender: Any)
    {
        if nameTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_PASSWORD)
        }
        else if emailTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_NEW_PASSWORD)
        }
        else if mobileNoTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_CONFIRM_PASSWORD)
        }
        else if mobileNoTxt.text != emailTxt.text
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PASSWORD_SAME)
        }
        else if nameTxt.text == emailTxt.text
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PASSWORD_NOT_SAME)
        }

//        else if passwordTxt.text?.isEmpty ?? true
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_PASSWORD)
//        }
//        else if confirmPasswordTxt.text?.isEmpty ?? true
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_CONFIRM_PASSWORD)
//        }
//        else if passwordTxt.text == confirmPasswordTxt.text
//        {
//            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PASSWORD_NOT_SAME)
//        }
        else
        {
            changePassword()
//            self.updateProfile()
        }
    }
    
    @IBAction func backAtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}

extension ChangePasswordViewController
{
    func setComponents()
    {
        registerTitleLbl.font = UIFont.appMediumFontWith(size: 16)
        nameTxt.font = UIFont.appRegularFontWith(size: 14)
        emailTxt.font = UIFont.appRegularFontWith(size: 14)
        mobileNoTxt.font = UIFont.appRegularFontWith(size: 14)
//        passwordTxt.font = UIFont.appRegularFontWith(size: 14)
//        confirmPasswordTxt.font = UIFont.appRegularFontWith(size: 14)
        alreadyAccountBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        signUpBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        
        registerTitleLbl.text = CONSTANT.CHANGE_PASSWORD
        nameTxt.placeholder = CONSTANT.CURRENT_PASSWORD
        emailTxt.placeholder = CONSTANT.NEW_PASSWORD
        mobileNoTxt.placeholder = CONSTANT.CONFIRM_PASSWORD
//        passwordTxt.placeholder = CONSTANT.CURRENT_PASSWORD
//        confirmPasswordTxt.placeholder = CONSTANT.NEW_PASSWORD
        alreadyAccountBtn.setTitle(CONSTANT.LOGOUT, for: .normal)
        signUpBtn.setTitle(CONSTANT.SAVE, for: .normal)

        
        nameTxt.isSecureTextEntry = true
        emailTxt.isSecureTextEntry = true
        mobileNoTxt.isSecureTextEntry = true
        
        nameTxt.keyboardType = .asciiCapable
        emailTxt.keyboardType = .asciiCapable
        mobileNoTxt.keyboardType = .asciiCapable

    }
    
    func changePassword()
    {
        let url =  APIList().getUrlString(url: .CHANGEPASSWORD)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userid": UserDefaults.standard.getUserId(),
            "oldPassword":nameTxt.text!,
            "password": mobileNoTxt.text!,
        ]
        
        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
}
