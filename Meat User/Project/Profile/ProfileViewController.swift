//
//  ProfileViewController.swift
//  Meat User
//
//  Created by Jai on 28/06/20.
//  Copyright © 2020 Jai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileViewController: BaseViewController
{
    
    @IBOutlet weak var registerTitleLbl: UILabel!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var mobileNoTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var alreadyAccountBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
    let connection = Connection()
    
    var dataArray = JSON()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setComponents()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getProfile()
    }
    
    @IBAction func alreadyHaveAtn(_ sender: Any)
    {
//        let vc = MainTabbarViewController.instantiate(fromAppStoryboard: .Main)
//        vc.modalPresentationStyle = .fullScreen
//        vc.modalTransitionStyle = .crossDissolve
//        self.present(vc, animated: true, completion: nil)

//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpAtn(_ sender: Any)
    {
        
        let vc = EditProfileViewController.instantiate(fromAppStoryboard: .Profile)
        vc.hidesBottomBarWhenPushed = true
        vc.dataArray = self.dataArray
        self.navigationController?.pushViewController(vc, animated: true)

        
        /*
        if nameTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_NAME_REGISTER)
        }
        else if emailTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_EMAIL)
        }
        else if mobileNoTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_MOBILE)
        }
        else if passwordTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_PASSWORD)
        }
        else if confirmPasswordTxt.text?.isEmpty ?? true
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.ENTER_CONFIRM_PASSWORD)
        }
        else if passwordTxt.text == confirmPasswordTxt.text
        {
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: CONSTANT.PASSWORD_NOT_SAME)
        }
        else
        {
            
        }*/
    }
    
    
}

extension ProfileViewController
{
    func setComponents()
    {
        registerTitleLbl.font = UIFont.appMediumFontWith(size: 16)
        nameTxt.font = UIFont.appRegularFontWith(size: 14)
        emailTxt.font = UIFont.appRegularFontWith(size: 14)
        mobileNoTxt.font = UIFont.appRegularFontWith(size: 14)
        passwordTxt.font = UIFont.appRegularFontWith(size: 14)
        confirmPasswordTxt.font = UIFont.appRegularFontWith(size: 14)
        alreadyAccountBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 14)
        signUpBtn.titleLabel?.font = UIFont.appMediumFontWith(size: 16)
        
        registerTitleLbl.text = CONSTANT.PROFILE
        nameTxt.placeholder = CONSTANT.NAME
        emailTxt.placeholder = CONSTANT.EMAIL
        mobileNoTxt.placeholder = CONSTANT.MOBILE_NO
        passwordTxt.placeholder = CONSTANT.CURRENT_PASSWORD
        confirmPasswordTxt.placeholder = CONSTANT.NEW_PASSWORD
        alreadyAccountBtn.setTitle(CONSTANT.LOGOUT, for: .normal)
        signUpBtn.setTitle(CONSTANT.EDIT_PROFILE, for: .normal)
        
        passwordTxt.isSecureTextEntry = true
        passwordTxt.text = "********"
    }
    
    func getProfile()
    {
        let url =  APIList().getUrlString(url: .VIEWPROFILE)
        
        let headers : HTTPHeaders = [
            "Content-Type":CONTENT_TYPE,
        ]
        
        let parameter : Parameters = [
            "userId": UserDefaults.standard.getUserId()
        ]

        showLottie()

        connection.requestPOST(url, params: parameter, headers: headers, success:
        {
            (data) in
            self.hideLottie()
            
            if data["error"].boolValue
            {
                self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: data["message"].stringValue)
            }
            else
            {
                self.setData(data: data)
            }
            
        })
        {
            (error) in
            self.hideLottie()
            self.showAlertError(titleStr: CONSTANT.OOPS, messageStr: error.localizedDescription)
        }
    }
    
    func setData(data: JSON)
    {
        self.dataArray = data
        nameTxt.text = data["data"]["custName"].stringValue
        emailTxt.text = data["data"]["custEmail"].stringValue
        mobileNoTxt.text = data["data"]["custPhone"].stringValue
    }
}
